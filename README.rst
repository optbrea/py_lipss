========================
py_lipss: simulation of gratings and lipss structures using RCWA (s4)
========================


.. image:: https://img.shields.io/pypi/v/py_lipss.svg
        :target: https://pypi.org/project/py-pol/

.. image:: https://img.shields.io/travis/optbrea/py_lipss.svg
        :target: https://bitbucket.org/optbrea/py_lipss/src/master/

.. image:: https://readthedocs.org/projects/py-pol/badge/?version=latest
        :target: https://py-pol.readthedocs.io/en/latest/
        :alt: Documentation Status



* Free software: MIT license
* Documentation: https://py-pol.readthedocs.io/en/latest/

.. image:: logo.png
   :width: 75
   :align: right



Features
----------------
Py-pol is a Python library for Jones and Stokes-Mueller polarization optics. It has 4 main module:

    * jones_vector - for generation of polarization states in 2x1 Jones formalism.

  
Examples
-----------

Jones 
=================

Generating Jones vectors and Matrices

.. code-block:: python

    from py_lipss.jones_vector import Jones_vector
    from py_lipss.jones_matrix import Jones_matrix
    from py_lipss.utils import degrees

    j0 = Jones_vector("j0")
    j0.linear_light(angle=45*degrees)

    m0 = Jones_matrix("m0")
    m0.diattenuator_linear( p1=0.75, p2=0.25, angle=45*degrees)

    m1 = Jones_matrix("m1")
    m1.quarter_waveplate(angle=0 * degrees)

    j1=m1*m0*j0



Drawings
===========

The modules also allows to obtain graphical representation of polarization.

Drawing polarization ellipse for Jones vectors.

.. image:: ellipse_Jones_1.png
   :width: 600


Authors
----------------

* Luis Miguel Sanchez Brea <optbrea@ucm.es>


    **Universidad Complutense de Madrid**,
    Faculty of Physical Sciences,
    Department of Optics
    Plaza de las ciencias 1,
    ES-28040 Madrid (Spain)

.. image:: logoUCM.png
   :width: 125
   :align: right

Citing
----------------
L.M. Sanchez Brea, "py-pol, python module for polarization optics", https://pypi.org/project/py-pol/ (2019)

References
------------

* D Goldstein "Polarized light" 2nd edition, Marcel Dekker (1993).


Acknowlegments
------------------------
This software was initially developed for the project Retos-Colaboración 2016 "Ecograb"  RTC-2016-5277-5: Ministerio de Economía y Competitivdad (Spain) and the European funds for regional development (EU), led by Luis Miguel Sanchez-Brea


Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
