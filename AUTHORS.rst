===========
Credits
===========

Development Lead
---------------------

* Luis Miguel Sanchez Brea <optbrea@ucm.es>

    **Universidad Complutense de Madrid**,
    Faculty of Physical Sciences,
    Department of Optics
    Plaza de las ciencias 1,
    ES-28040 Madrid (Spain)

    .. image:: logoUCM.png
       :width: 125
       :align: right

Contributors
--------------

None yet. Why not be the first?
