#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module is to generate standard structures not defined by functions or shapes.
"""

um = 1.


def planar_structure(materials_names, thicknesses):

    layers = []

    for i in range(len(thicknesses)):
        layer_name = "l{}".format(i)
        layer = dict(Name=layer_name,
                     Material=materials_names[i],
                     Thickness=thicknesses[i])

        layers.append(layer)

    patterns = []
    return layers, patterns


def multilayer(ncs, depths, material_names):
    """Generates the the structure of a multilayer.


    Parameters:
        ncs: array with refraction index
        names: array with names
        depths: array with depths
        material_names: array with materials:

    Example of parameters:
        ncs=[1, 1.5+0.01j, 2.25, 1]
        material_names= ["Vaccum",  "material_1", "material_2", "Vacuum"]
        depths=[0,2, 2,0]

    """
    layers = []

    for i, name in enumerate(material_names):
        dictionary = {}
        dictionary["Name"] = "l" + str(i)
        dictionary["Thickness"] = depths[i]
        dictionary["Material"] = material_names[i]
        layers.append(dictionary)

    return layers


def binary_grating(materials_names, period, height, fill_factor):
    layers = [
        {
            "Name": "l0",
            "Thickness": 0.,
            "Material": materials_names[0]
        },
        {
            "Name": "l1",
            "Thickness": height,
            "Material": materials_names[0]
        },
        {
            "Name": "l2",
            "Thickness": 0.,
            "Material": materials_names[0]
        },
    ]

    patterns = [{
        "Rectangle": {
            "Layer": "l1",
            "Material": materials_names[1],
            "Center": (0, 0),
            "Angle": 0.,
            "Halfwidths": (period * fill_factor / 2., 0)
        }
    }]

    return layers, patterns


def binary_grating_with_substrate(materials_names, period, height, fill_factor):
    layers = [
        {
            "Name": "l0",
            "Thickness": 0.,
            "Material": materials_names[0]
        },
        {
            "Name": "l1",
            "Thickness": height,
            "Material": materials_names[0]
        },
        {
            "Name": "l2",
            "Thickness": 0.,
            "Material": materials_names[1]
        },
    ]

    patterns = [{
        "Rectangle": {
            "Layer": "l1",
            "Material": materials_names[1],
            "Center": (0, 0),
            "Angle": 0.,
            "Halfwidths": (period * fill_factor / 2., 0)
        }
    }]

    return layers, patterns


def from_LUT(grating, materials, height_structure, LUT, invert=False):
    """Insert the LUT structure in the XZ simulation.

    Arguments:
        grating (nanoxz.NanoArray2D): Simulation
        materials (dict): Dictionary with the two materiales
        height_structure (float): height structure in microns
        LUT (np.array 2D) binary structure generated with a 2D

    Returns:
        grating
    """

    num_layers = LUT.shape[0]
    thickness = height_structure / num_layers

    if invert is True:
        layers = [{"Name": "l0", "Thickness": 0, "Material": "Vacuum"}]
        for i in range(1, num_layers + 1):
            layers.append({
                "Name": "l{}".format(i),
                "Thickness": thickness,
                "Material": "Vacuum"
            })
        layers.append({
            "Name": "l{}".format(i + 1),
            "Thickness": 0.,
            "Material": "Material"
        })

    else:
        layers = [{"Name": "l0", "Thickness": 0, "Material": "Material"}]
        for i in range(1, num_layers + 1):
            layers.append({
                "Name": "l{}".format(i),
                "Thickness": thickness,
                "Material": "Vacuum"
            })
        layers.append({
            "Name": "l{}".format(i + 1),
            "Thickness": 0.,
            "Material": "Vacuum"
        })

    mater_lut = {"Vacuum": 0, "Material": 1}

    # Define the structure using py_lipss
    grating.set_materials(materials=materials)
    grating.set_layers(layers)

    grating.add_pattern(LUT, range(1, num_layers + 1), mater_lut)
    # grating.set_patterns(LUT)

    return grating
