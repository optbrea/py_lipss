#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np

from .plotters import draw_field, plot2D_EH, plot2D_eps, plot_E2H2

# def get_E_0D(grt, no_grating, no_cell, kind='reflected'):
#     """Get E and H returning 1D fields. This is useful for polation in subwavelength structures (using get_stokes_from_E)
#
#     Parameters:
#         sim (simS4.Patterned): structure defined and with data
#
#     Returns:
#         E_0D (np.array): 1x3 E field
#
#     Warning:
#         TOTEST
#     """
#     if kind == 'reflected':
#         E = grt.fields[:, 0, 0, 0] - no_cell.fields[:, 0, 0, 0]
#     elif kind == 'transmitted':
#         E = grt.fields[:, 0, 0, 0]
#
#     E_0D = E.sum(axis=0)
#     # R = -grt.fluxes[0, :, 1].sum().real
#     # R0 = -no_cell.fluxes[0, :, 1].sum().real
#     # ax2.title.set_text("Output R={:2.2f}, Eff. {:2.2f} %".format(
#     #     R, 100 * R / R0))
#     return E_0D


def compute_3_structures(grating, has_no_grating=False, has_no_cell=False):
    """Function to calculate the results stored in the grating pyS4 class for the nanoestructure, the planar and the empty. These two last structures are required form comparison. For example, to remove the incident field in polarization computations and to compare the efficiency of the nanoestructure comparison with the plane interaface.

    Parameters:
        grating (simS4.Patterned)

    Returns:
        grating (simS4.Patterned:      fields with the nanoestructure
        no_grating (simS4.Patterned):  field without the nanoestructure, just the plane surfaces.
        no_cell (simS4.Patterned):     field at vaccum, with no material.

    """
    grating.stop()
    grating.start(run_calcs=True)

    if has_no_grating:
        no_grating = grating.empty_pattern()
        no_grating.start(run_calcs=True)
    else:
        no_grating = None

    if has_no_cell:
        no_cell = grating.empty()
        no_cell.start(run_calcs=True)
    else:
        no_cell = None

    return grating, no_grating, no_cell


def get_E_1D(grt, no_grating, no_cell, kind='reflected'):
    """Get E and H returning 1D fields. This is useful for polation in subwavelength structures (using get_stokes_from_E)

    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        E_0D (np.array): 1x3 E field

    Warning:
        TOTEST
    """
    if kind == 'reflected':
        E = grt.fields[:, 0, 0, 0] - no_cell.fields[:, 0, 0, 0]
    elif kind == 'transmitted':
        E = grt.fields[:, 0, 0, 0]
    return E


def get_EH(fields, xyz=None, has_draw=False, axis='equal'):
    """Determines the fields from the fields parameter

    xyz=z0S

    """

    E = fields[:, :, 0, 0, :]
    H = fields[:, :, 0, 1, :]

    Ex = E[:, :, 0].squeeze()
    Ey = E[:, :, 1].squeeze()
    Ez = E[:, :, 2].squeeze()
    Hx = H[:, :, 0].squeeze()
    Hy = H[:, :, 1].squeeze()
    Hz = H[:, :, 2].squeeze()

    S_max = np.real(
        np.max((Ex.real, Ey.real, Ez.real, Hx.real, Hy.real, Hz.real)))
    S_min = np.real(
        np.min((Ex.real, Ey.real, Ez.real, Hx.real, Hy.real, Hz.real)))

    if has_draw:
        plot2D_EH(E, H, xyz=xyz, axis=axis)

    return E, H


def get_EH_2D(sim, z=None, fields_og=False, **kwargs):
    """Get E and H returning 3D fields

    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        E (np.array): *x* E field
        H (np.array): *x* H field

    Warning:
        TOTEST
    """

    if fields_og:
        fields = sim.fields_og
        E = fields[:, 0, :, 0, :]
        H = fields[:, 1, :, 0, :]
    else:
        fields = sim.fields
        E = fields[:, :, 0, 0, :]
        H = fields[:, :, 0, 1, :]
    if z is None:
        z = sim.fields_monitors[2]

    return E, H


def get_EH__total_2D(sim, z=None, fields_og=False, **kwargs):
    """Get E and H returning 3D fields

    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        E (np.array): *x* E field
        H (np.array): *x* H field

    Warning:
        TOTEST
    """

    if fields_og:
        fields = sim.fields_og
        E = fields[:, 0, :, 0, :]
        H = fields[:, 1, :, 0, :]
    else:
        fields = sim.fields
        E = fields[:, :, 0, 0, :]
        H = fields[:, :, 0, 1, :]
    if z is None:
        z = sim.fields_monitors[2]
    E = np.linalg.norm(E, axis=-1)
    H = np.linalg.norm(H, axis=-1)

    return E, H


def get_EH_3D(sim, z=None):
    """Get E and H returning 3D fields

    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        E (np.array): *x* E field
        H (np.array): *x* H field

    Warning:
        it had z, fields_og, **kwargs, but were not used

    Warning:
        TOTEST
    """
    Ex = sim.results['fields'][:, :, 0, 0, 0].squeeze()
    Ey = sim.results['fields'][:, :, 0, 0, 1].squeeze()
    Ez = sim.results['fields'][:, :, 0, 0, 2].squeeze()

    Hx = sim.results['fields'][:, :, 0, 1, 0].squeeze()
    Hy = sim.results['fields'][:, :, 0, 1, 1].squeeze()
    Hz = sim.results['fields'][:, :, 0, 1, 2].squeeze()

    d1, d2 = np.shape(Ex)

    E = np.zeros((d1, d2, 3), dtype=complex)
    H = np.zeros((d1, d2, 3), dtype=complex)
    E[:, :, 0] = Ex
    E[:, :, 1] = Ey
    E[:, :, 2] = Ez
    H[:, :, 0] = Hx
    H[:, :, 1] = Hy
    H[:, :, 2] = Hz

    return E, H


def get_eps(sim, xyz=None, has_draw=False, axis='scaled'):
    """Determines the dielectric constant in terms of position


    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        eps_real (np.array): real part of dielectric constant
        eps_imag (np.array). imaginary part of dielectric constant

    Warning:
        it had z, fields_og, **kwargs, but were not used

    Warning:
        TOTEST
    """
    x0, z0 = xyz
    # dims = np.shape(Ex)
    # num_dims = len(dims)
    # if z0 is None:
    #     z0 = np.linspace(0, 1, dims[1])
    # if x0 is None:
    #     x0 = np.linspace(0, 1, dims[0])

    # eps = sim.eps[:, :, 0].squeeze()
    eps = sim.get_epsilon(x=x0, z=z0).squeeze()

    if has_draw:
        plot2D_eps(eps, xyz, axis)

    return eps


# def get_refraction_index(sim):
#     """Determines the refraction index in terms of position
#
#
#     Parameters:
#         sim (simS4.Patterned): structure defined and with data
#
#     Returns:
#         n_real (np.array): real part of refraction index
#         n_imag (np.array). imaginary part refraction index
#
#     Warning:
#         it had z, fields_og, **kwargs, but were not used
#
#     Warning:
#         TOTEST, TODO
#     """
#
#     eps = sim.eps[:, :, 0].squeeze()
#     eps_real = np.real(eps)
#     eps_imag = np.imag(eps)
#
#     n_real = np.ones_like(eps_real)
#     n_imag = np.zeros_like(eps_imag)
#
#     return n_real, n_imag


def get_diffraction_orders(sim):
    """Determines the refraction index in terms of position


    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        n_real (np.array): real part of refraction index
        n_imag (np.array). imaginary part refraction index

    Warning:
        it had z, fields_og, **kwargs, but were not used

    Warning:
        TOTEST, TODO
    """
    glist = sim.glist
    fluxes_out = sim.fluxes

    efficiency_reflectance = -fluxes_out[0, :, 1]
    efficiency_transmitance = -fluxes_out[1, :, 1]

    glist = glist[:, 0]

    return glist, efficiency_reflectance, efficiency_transmitance


def get_amplitudes(sim):
    """Determines the amplitudes of the reflected field?


    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        amplitudes (np.array): Reflected amplitudes of the field

    Warning:
        TOTEST, TODO
    """
    #glist = sim.glist
    #amps_out = sim.amps
    amps_out = sim.get_amps()
    glist = sim.glist[:, 0]

    num_orders = glist.shape[0]

    amplitudes_reflection = -amps_out[0, 1, :num_orders]
    amplitudes_transmission = amps_out[0, 0, :num_orders]
    return glist, amplitudes_reflection, amplitudes_transmission


# Reflectance
def get_efficiency_reflectance(exp, exp0=None, verbose=True):

    R = -exp.fluxes[0, :, 1].sum().real

    if exp0 is not None:
        R0 = -exp0.fluxes[0, :, 1].sum().real
        efficiency = R / R0

    else:
        R0 = None
        efficiency = None

    if verbose is True:
        print("Output R={0:.2f}, R0={0:.2f}, eff={1:.2f}%".format(
            R, R0, efficiency))

    return R, R0, efficiency


# Transmitance
def get_efficiency_transmitance(exp, exp0=None, verbose=True):
    T = exp.fluxes[2, :, 0].sum().real

    if exp0 is not None:
        T0 = exp0.fluxes[2, :, 0].sum().real
        efficiency = T / T0

    else:
        T0 = None
        efficiency = None

    if verbose is True:
        print("Output T={0:.2f}, T0={0:.2f}, eff={1:.2f}%".format(
            T, T0, efficiency))

    return T, T0, efficiency


def efficiencies(exp, exp0='', verbose=True):
    R, R0, eff_R = get_efficiency_reflectance(exp, exp0, verbose)
    T, T0, eff_T = get_efficiency_transmitance(exp, exp0, verbose)

    if verbose is True:
        print("____________________________________")
        print("reflectance  = {:2.4f}".format(R))
        print("transmitance = {:2.4f}".format(T))
        print("total        = {:2.4f}\n".format(R + T))
        print("efficiencies: R = {:2.4f} T = {:2.4f}".format(eff_R, eff_T))

    return R, T, R0, T0, eff_R, eff_T


def E2H2(E, H, xyz=None, has_draw=True, axis='equal'):
    E2 = np.linalg.norm(E, axis=-1)**2
    H2 = np.linalg.norm(H, axis=-1)**2

    if has_draw:
        plot_E2H2(E, H, xyz, axis=axis)

    return E2, H2


def Poynting_vector(E, H, xyz=None, has_draw=True, axis='equal'):
    "Instantaneous Poynting Vector"

    Ex = E[:, :, 0].squeeze()
    Ey = E[:, :, 1].squeeze()
    Ez = E[:, :, 2].squeeze()
    Hx = H[:, :, 0].squeeze()
    Hy = H[:, :, 1].squeeze()
    Hz = H[:, :, 2].squeeze()

    Sx = Ey * Hz - Ez * Hy
    Sy = Ez * Hx - Ex * Hz
    Sz = Ex * Hy - Ey * Hx

    S_max = np.real(np.max((Sx, Sy, Sz)))
    S_min = np.real(np.min((Sx, Sy, Sz)))
    S_lim = np.max((abs(S_max), np.abs(S_min)))

    if has_draw:
        dims = np.shape(Ex)
        num_dims = len(dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(1, 3, 1)
            plt.plot(z0, np.real(Sx))
            plt.ylim(-S_lim, S_lim)
            plt.title("$S_x$")

            plt.subplot(1, 3, 2)
            plt.plot(z0, np.real(Sy))
            plt.ylim(-S_lim, S_lim)
            plt.title("$S_y$")

            plt.subplot(1, 3, 3)
            plt.plot(z0, np.real(Sz))
            plt.ylim(-S_lim, S_lim)
            plt.title("$S_z$")

        elif num_dims == 2:
            x0, z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[1])
            if x0 is None:
                x0 = np.linspace(0, 1, dims[0])

            fig, axes = plt.subplots(nrows=3, ncols=1)
            plt.subplot(3, 1, 1)
            plt.title("$S_x$")
            draw_field(np.real(Sx), x0, z0, axis, cmap='seismic')
            plt.clim(-S_lim, S_lim)

            plt.subplot(3, 1, 2)
            plt.title("$S_y$")
            draw_field(np.real(Sy), x0, z0, axis, cmap='seismic')
            plt.clim(-S_lim, S_lim)

            plt.subplot(3, 1, 3)
            im3 = draw_field(np.real(Sz), x0, z0, axis, cmap='seismic')
            plt.clim(-S_lim, S_lim)
            plt.title("$S_z$")
            plt.xlabel('z ($\mu$m)')

            cb_ax = fig.add_axes([0.1, 0, 0.8, 0.05])
            cbar = fig.colorbar(im3, cax=cb_ax, orientation='horizontal')

        plt.suptitle("Instantaneous Pointing vector")

    return Sx, Sy, Sz


def Poynting_vector_averaged(E, H, xyz=None, has_draw=False, axis='scaled'):
    "Averaged Poynting Vector"

    Ex = E[:, :, 0]
    Ey = E[:, :, 1]
    Ez = E[:, :, 2]
    Hx = H[:, :, 0]
    Hy = H[:, :, 1]
    Hz = H[:, :, 2]

    Sx = np.real(Ey * Hz.conjugate() - Ez * Hy.conjugate()).squeeze()
    Sy = np.real(Ez * Hx.conjugate() - Ex * Hz.conjugate()).squeeze()
    Sz = np.real(Ex * Hy.conjugate() - Ey * Hx.conjugate()).squeeze()

    # Sx = np.abs(Sx)
    # Sy = np.abs(Sy)
    # Sz = np.abs(Sz)

    S_max = np.max((Sx, Sy, Sz))
    S_min = np.min((Sx, Sy, Sz))
    S_lim = np.max((abs(S_max), np.abs(S_min)))

    if has_draw:
        dims = np.shape(Sx)
        num_dims = len(dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(3, 1, 1)
            plt.plot(z0, Sx)
            plt.ylim(-S_lim, S_lim)
            plt.title("$S_x$")

            plt.subplot(3, 1, 2)
            plt.plot(z0, Sy)
            plt.title("$S_y$")
            plt.ylim(-S_lim, S_lim)

            plt.subplot(3, 1, 3)
            plt.plot(z0, Sz)
            plt.title("$S_z$")
            plt.ylim(-S_lim, S_lim)

            plt.suptitle("Average Pointing vector")

        elif num_dims == 2:
            x0, z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[1])
            if x0 is None:
                x0 = np.linspace(0, 1, dims[0])

            fig, axes = plt.subplots(nrows=3, ncols=1)
            plt.subplot(3, 1, 1)
            plt.title("$S_x$")
            im1 = draw_field(Sx, x0, z0, axis, cmap='seismic')
            plt.clim(-S_lim, S_lim)
            axes[0].set_axis_off()

            plt.subplot(3, 1, 2)
            plt.title("$S_y$")
            im2 = draw_field(Sy, x0, z0, axis, cmap='seismic')
            plt.clim(-S_lim, S_lim)
            axes[1].set_axis_off()

            plt.subplot(3, 1, 3)
            im3 = draw_field(Sz, x0, z0, axis, cmap='seismic')
            plt.title("$S_z$")
            plt.clim(-S_lim, S_lim)
            axes[2].set_axis_off()

            # = fig.colorbar(im3, ax=axes.ravel().tolist(), shrink=0.95)
            cb_ax = fig.add_axes([0.1, 0, 0.8, 0.05])
            cbar = fig.colorbar(im3, cax=cb_ax, orientation='horizontal')

    return Sx, Sy, Sz


def Poynting_total(E, H, xyz=None, has_draw=False, axis='equal'):

    Sx, Sy, Sz = Poynting_vector_averaged(E, H, xyz=xyz, has_draw=False)

    S = np.sqrt(np.abs(Sx)**2 + np.abs(Sy)**2 + np.abs(Sz)**2)

    if has_draw:
        dims = np.shape(Sx)
        num_dims = len(dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(1, 1, 1)
            plt.plot(z0, S)

            plt.suptitle("$S_{total}$")
        elif num_dims == 2:
            x0, z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[1])
            if x0 is None:
                x0 = np.linspace(0, 1, dims[0])

            x0, z0 = xyz
            draw_field(S, x0, z0, axis)
            plt.colorbar(orientation='vertical')
            plt.suptitle("$S_{total}$")
            plt.clim(vmin=0)

    return S


def energy_density(E, H, epsilon, xyz=None, has_draw=False, axis='equal'):
    Ex = E[:, :, 0].squeeze()
    Ey = E[:, :, 1].squeeze()
    Ez = E[:, :, 2].squeeze()
    Hx = H[:, :, 0].squeeze()
    Hy = H[:, :, 1].squeeze()
    Hz = H[:, :, 2].squeeze()

    permeability = 4 * np.pi * 1e-7

    U = epsilon * (np.abs(Ex)**2 + np.abs(Ey)**2 + np.abs(Ez)**2)
    +permeability * (np.abs(Hx)**2 + np.abs(Hy)**2 + np.abs(Hz)**2)

    if has_draw:
        dims = np.shape(Ex)
        num_dims = len(dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(1, 1, 1)
            plt.plot(z0, np.real(U))

        elif num_dims == 2:
            x0, z0 = xyz
            draw_field(np.real(U), x0, z0, axis)
            plt.colorbar(orientation='vertical')

        plt.title("energy_density")
    return U


def Scattering(E, H, E_empty, H_empty):
    pass


def index_to_permitivity(n_complex):
    """permitivity of a material.

    Arguments:
        n_complex (np.float or np.complex): refraction index of a material

    returns:
        (np.complex): permitivity of material.
    """

    n_index = n_complex.real
    k_index = n_complex.imag

    eps_material = n_index**2 - k_index**2 + 2 * n_index * k_index * 1j

    return eps_material
