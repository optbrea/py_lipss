#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Module to determine positions to create shapes in form of layers

INFO: function names changed
    drawing_cos -> LUT_cos
    drawing_cos2 -> LUT_cos2
    drawing_zxfun -> LUT_zxfun
    drawing_halfcos -> LUT_halfcos
    drawing_triangle -> LUT_triangle
    drawing_cricle -> LUT_circle

"""

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp
import scipy.interpolate

from .plotters import draw_field, draw_LUT

um = 1.


def find_nearest(array, value):
    """ rutine to find the nearest value in a numpy array"""
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx


def edges(a, v, *args, **kwargs):
    """ Function to return the edges in a True-False array:

    Arguments:
        a: int value of the mask
        v: the array
    """
    lut = a == v
    dZ = lut[1:] ^ lut[:-1]
    j, = np.where(dZ)
    return j


def LUT_xzfun(x0,
              z0,
              num_layers,
              amplitude=1.,
              xres=10,
              offset=0,
              kind='linear',
              invert=False,
              has_draw=False):
    """ Function to generate a 2d image, z = f(x). Between the maximum
    and minium value the function values are set
    to True, and upper to False. The minimum value in z will be the floor of the
    function, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis.
    Args:
        x0 (np.array): x-direction points
        z0 (np.array): z-direction points
        num_layers (int): Number of layers (discretization in height, z)
        amplitude (float, 0-1): Normalized Amplitude of the function
        xres (int >=1) : Discretization in x
        zoffset(float): Offset in z direction of the function.
    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (num_layers, xres*num_layers)
    """
    x = np.linspace(x0.min(), x0.max(), num_layers * xres)
    z = np.linspace(0, 1., num_layers)

    X, Z = np.meshgrid(x, z)
    z0 = -z0
    z0 = (z0 - z0.min())
    z0 = z0 / z0.max()
    F = sp.interpolate.interp1d(x0, z0, kind=kind, bounds_error=None)
    LUT = Z - F(X) + offset >= 0

    if invert:
        LUT = np.flipud(LUT)

    if has_draw is True:
        draw_LUT(LUT, x, z, axis='scaled', cmap='gray')

    return np.array(LUT, dtype='bool')


def LUT_cos(num_layers,
            period,
            amplitude=1.,
            xres=10,
            offset=0,
            invert=False,
            has_draw=False):
    """ Function to generate a cos/sin 2d image, z = cos(x). Under the cosine values are set
    to True, and upper to False, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis. This is useful
    for RCWA .
    Args:
        num_layers (int): Number of layers (discretization in height, z)
        period (float, 0-1): Normalized period of the cos function
        amplitude (float, 0-1): Normalized Amplitude of the cos function
        xres (int >=1) : Discretization in x
        zoffset(float): Offset in z direction of the cos function.
        has_draw (bool): If True, draws shape
    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (num_layers, xres*num_layers)
    """
    x = np.linspace(-0.5, 0.5, num_layers * xres)
    z = np.linspace(0, 1., num_layers)
    X, Z = np.meshgrid(x, z)

    func = amplitude / 2 * (-1 - (np.cos(2 * np.pi * X)))
    LUT = Z + func < 0
    # LUT[abs(X) > period] = False  # Only one structure within the unit-cell

    if invert:
        LUT = np.flipud(LUT)

    if has_draw is True:
        draw_LUT(LUT, x, z, axis='scaled', cmap='gray')

    return np.array(LUT, dtype='bool')


def LUT_cos_beta(num_layers,
                 amplitude=1,
                 xres=10,
                 beta=1,
                 invert=False,
                 has_draw=False):
    """ Function to generate a cos/sin 2d image, z = cos(x). Under the cosine values are set
    to True, and upper to False, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis. This is useful
    for RCWA .
    Args:
        num_layers (int): Number of layers (discretization in height, z)
        amplitude (float, 0-1): Normalized Amplitude of the cos function
        xres (int >=1) : Discretization in x
        has_draw (bool): If True, draws shape

    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (num_layers, xres*num_layers)
    """
    x = np.linspace(-0.5, 0.5, num_layers * xres)
    z = np.linspace(0, 1., num_layers)
    X, Z = np.meshgrid(x, z)

    func = amplitude * (-1 + np.abs(np.sin(2 * np.pi * X / 2))**beta)
    LUT = Z + func < 0
    # LUT[abs(X) > period] = False  # Only one structure within the unit-cell

    if invert:
        LUT = np.flipud(LUT)

    if has_draw is True:
        draw_LUT(LUT, x, z, axis='scaled', cmap='gray')

    return np.array(LUT, dtype='bool')


def LUT_supergauss(num_layers,
                   width=0.5,
                   beta=2,
                   xres=10,
                   invert=False,
                   has_draw=False):
    """ Function to generate a cos/sin 2d image, z = cos(x). Under the cosine values are set
    to True, and upper to False, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis. This is useful
    for RCWA .
    Args:
        num_layers (int): Number of layers (discretization in height, z)
        amplitude (float, 0-1): Normalized Amplitude of the cos function
        xres (int >=1) : Discretization in x
        invert (bool): Flips the structure
        has_draw (bool): If True, draws shape

    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (num_layers, xres*num_layers)
    """
    x = np.linspace(-0.5, 0.5, num_layers * xres)
    z = np.linspace(0, 1., num_layers)
    X, Z = np.meshgrid(x, z)

    func = np.exp(-np.abs(X / (0.5 * width))**beta)
    # print(func.max(), func.min())
    LUT = Z - func < 0
    if invert:
        LUT = np.flipud(LUT)
    # LUT[abs(X) > period] = False  # Only one structure within the unit-cell

    if invert:
        LUT = np.flipud(LUT)

    if has_draw is True:
        draw_LUT(LUT, x, z, axis='scaled', cmap='gray')

    return np.array(LUT, dtype='bool')


def LUT_halfcos(num_layers,
                fill_factor,
                xres=10,
                invert=False,
                has_draw=False):
    x = np.linspace(-0.5, 0.5, num_layers * xres)
    z = np.linspace(0, 1, num_layers)
    X, Z = np.meshgrid(x, z)

    func = np.cos(np.pi * X / fill_factor)
    LUT = Z - func < 0
    # Only one structure within the unit-cell
    LUT[abs(X) > fill_factor] = False

    if invert:
        LUT = np.flipud(LUT)

    if has_draw is True:
        draw_LUT(LUT, x, z, axis='scaled', cmap='gray')

    return np.array(LUT, dtype='bool')


def LUT_triangle(num_layers,
                 fill_factor,
                 xres=10,
                 invert=False,
                 has_draw=False):
    x = np.linspace(-0.5, 0.5, num_layers * xres)
    z = np.linspace(0, 1, num_layers)
    X, Z = np.meshgrid(x, z)

    func = 1 - 2 * abs(X) / fill_factor
    LUT = Z - func < 0

    if invert:
        LUT = np.flipud(LUT)

    if has_draw is True:
        draw_LUT(LUT, x, z, axis='scaled', cmap='gray')

    return np.array(LUT, dtype='bool')


def LUT_blazed_grating(num_layers, angle_0, angle_1, xres=10, invert=False, has_draw=False):
    """Defines a blazed grating with angles angle_0 and angle_1.

    Arguments:
        x0 (np.array): x positions
        angle_0 (float): angle in radians for the left side
        angle_1 (float): angle in radians for the right side

    returns:
        topography (np.array): shape of the grating

    example:
        LUT_blazed_grating(51,22*degrees, 45*degrees, xres=10,invert=True, has_draw=True)
    """
    x = np.linspace(0, 1, num_layers * xres)
    z = np.linspace(0, 1, num_layers)
    X, Z = np.meshgrid(x, z)

    z_left = np.tan(angle_0) * x
    z_right = np.tan(angle_1) * (1 - x)
    func = np.minimum(z_left, z_right)

    LUT = Z < func

    if invert:
        LUT = np.flipud(LUT)

    if has_draw is True:
        draw_LUT(LUT, x, z, axis='scaled', cmap='gray')

    return np.array(LUT, dtype='bool')


def LUT_circle(num_layers,
               z0=0,
               radius=(0.5, 0.5),
               xres=10,
               invert=False,
               has_draw=False):

    x = np.linspace(-0.5, 0.5, num_layers * xres)
    z = np.linspace(0, 1, num_layers)
    X, Z = np.meshgrid(x, z)
    func = (X / radius[0])**2 + ((Z - z0) / radius[1])**2

    LUT = func < 1

    if invert:
        LUT = np.flipud(LUT)

    if has_draw is True:
        draw_LUT(LUT, x, z, axis='scaled', cmap='gray')

    return np.array(LUT, dtype='bool')


def get_edges_LUT(LUT):
    dZ = LUT[:, :-1] ^ LUT[:, 1:]

    return dZ
