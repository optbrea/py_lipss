# -*- coding: utf-8 -*-
"""Top-level package for Python LIPSS."""

__author__ = """Luis Miguel Sanchez Brea / Jerónimo Buencuerpo"""
__email__ = 'optbrea@ucm.es'
__version__ = '0.0.31'

import numpy as np
import scipy as sp

name = 'py_lipss'

um = 1.
mm = 1000 * um
nm = um / 1000.
degrees = np.pi / 180

eps = 1e-8
num_decimals = 4
