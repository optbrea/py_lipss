#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
pyS4:
-----

Python bindings around the S4 package using a more
object oriented approach. The fundmental class is the Sim class with bindings
to recreate an structure.
"""
