#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import collections
from copy import copy, deepcopy
from functools import wraps

import numpy as np
# from . import S4
import S4


# DECORATOR FUNCTION TO AUTOSTART THE INIT FUNCTIONS IF IT IS REQUIRED
def deco_start(fun):
    @wraps(fun)
    def inner(*args, **kwargs):
        fun(*args, **kwargs)
        if args[0].autostart:
            # inner.__name__=f.__name__
            # inner.__doc__=f.__doc__
            f = getattr(args[0], "_{}".format(fun.__name__))
            return f()

    return inner


class _SimS4(object):
    def __init__(self, **kwargs):
        self.S = None
        self.results = dict()  # dictionary of the results
        self.queries = set()  # queries
        self.sim_info = set()  # sim_info
        self.setters = collections.OrderedDict(
            (("new", None), ("materials", None), ("layers", None), ("excite",
                                                                    None)))
        # setters will define the autostart and restart functions,
        # child classes only need to add setters to reproduce the
        # simulation. It is an ordereddict as it is needed
        self.autostart = None

    def _set_new(self, **kwargs):
        self.S = S4.New(**self.unit_cell)
        self.S.SetOptions(**self.sim_options)

    def _set_materials(self, **kwargs):
        if isinstance(self.materials, (list, tuple)):
            [self.S.SetMaterial(**mater) for mater in self.materials]
        elif isinstance(self.materials, dict):
            [
                self.S.SetMaterial(*(k, mater))
                for k, mater in self.materials.items()
            ]
        else:
            raise TypeError('Material data not understood,\
            {}'.format(self.materials))

    def _set_layers(self, **kwargs):
        if isinstance(self.layers[0], (dict)):
            [self.S.AddLayer(**layer) for layer in self.layers]
        elif isinstance(self.layers[0], (list, tuple)):
            [self.S.AddLayer(*layer) for layer in self.layers]
        else:
            raise TypeError('Layer data not understood,\
            {}'.format(self.layers))

    def _set_excite(self):
        self.S.SetExcitationPlanewave(**self.source)
        self.S.SetFrequency(self.freq)
        self.S.GetBasisSet()  # If this is not introduced the flux return fails

    def _z_monitor(self, monitors):
        if monitors is None:
            fluxes = [{
                "Layer": layer["Name"],
                "zOffset": 0
            } for layer in self.layers]
        else:
            fluxes = monitors

        return fluxes

    def _flux0(self):
        # TODO OPTIMIZE FLUX0
        # HARDCODED l0 ALWAYS FIRST LAYER
        flux0 = {"Layer": "l0", "zOffset": 0}
        self.flux0 = np.array(
            self.S.GetPowerFlux(**flux0))[0]  # Normalization for out of
        return self.flux0.real

    def _get_glist(self):
        self.glist = np.array(self.S.GetBasisSet())
        self.results["glist"] = self.glist
        return self.glist

    def _get_fluxes(self, **kwargs):
        self.flux0 = self._flux0
        if isinstance(self.fluxes_monitors[0], dict):
            self.fluxes = [
                self.S.GetPowerFluxByOrder(**mon)
                for mon in self.fluxes_monitors
            ]

        elif isinstance(self.fluxes_monitors[0], list):
            self.fluxes = [
                self.S.GetPowerFluxByOrder(*mon)
                for mon in self.fluxes_monitors
            ]
        # print self.fluxes
        self.fluxes = np.array(self.fluxes) / self._flux0()
        self.results['fluxes'] = self.fluxes
        self._get_glist()
        return self.glist, self.fluxes

    def _get_amps(self, **kwargs):
        self.amps = [self.S.GetAmplitudes(**amp) for amp in self.amps_monitors]
        self.amps = np.array(self.amps)  # /self._flux0()
        self.results['amps'] = self.amps
        return self.amps

    def _get_fields_og(self, **kwargs):
        # for monitor in self.fields_ogmonitors:
        # E,H = self.S.GetFieldsOnGrid(**monitor)
        # E,H = np.array(E), np.array(H)
        # fields = np.array((E,H))
        fields = [
            self.S.GetFieldsOnGrid(**monitor)
            for monitor in self.fields_ogmonitors
        ]
        fields = np.array(fields)
        # fields = np.resize(fields, (fields.shape[0],fields.shape[2],
        # fields.shape[3],fields.shape[1],
        # fields.shape[4]))
        self.fields_og = np.array(fields)
        self.results["fields_og"] = self.fields_og
        return self.fields_og

    def _get_fields(self, **kwargs):
        xyz = self.fields_monitors
        self.fields = np.array(
            [[[self.S.GetFields(xi, yi, zi) for yi in xyz[1]] for xi in xyz[0]]
             for zi in xyz[2]])
        self.results['fields'] = self.fields
        return self.fields

    def _get_epsilon(self, **kwargs):
        xyz = self.eps_monitor
        self.eps = np.array([[[self.S.GetEpsilon(xi, yi, zi) for yi in xyz[1]]
                              for xi in xyz[0]] for zi in xyz[2]])
        self.epsilon = self.eps  # for backward comptatibility
        self.results["epsilon"] = self.epsilon
        return self.epsilon

    def _set_patterns(self):
        # print self.patterns
        if len(self.patterns) > 0:

            for pattern in self.patterns:
                keys = list(pattern.keys())
                pattype = keys[0]
                getattr(self.S,
                        "SetRegion{}".format(pattype))(**pattern[pattype])

    def templates(self):
        """Function to  help the creation of the Simulation cell:

        Returns:
            template (dict): keys:values appropiate for set and get calls.

        """
        template = {}
        layers = [
            {
                "Name": "l0",
                "Thickness": 0.,
                "Material": "Vacuum"
            },
            {
                "Name": "l1",
                "Thickness": 0.,
                "Material": "Vacuum"
            },
        ]
        materials = {"Vacuum": 1.0, "SiO2": 1.5**2}
        monitors = [{"Layer": "l0", "zOffset": 0}]
        template = {
            "layers": layers,
            "materials": materials,
            "monitors": monitors,
        }
        return template


class Sim(_SimS4):
    """Class to create an S4 simulation object.

    This class does not contain any details concerning the structure to be simulated.
    To define further structures, redefine set_structure, and start (???)

    Attibutes:
        :_a: First lattice vector.
        :_b: Second lattice vector.
        :_center: Numpy array with the center of the unit cell.
        :_G: Number of plane waves.
        :S: S4 simulation object.
        unit_cell: Dictionary containing the unit cell details: lattice and plane waves.
            These parameters are the arguments of the S4 constructor.



    """

    def __init__(self,
                 a,
                 G=1,
                 b=None,
                 sim_options={"Verbosity": 0},
                 autostart=True,
                 **kwargs):
        """Init Sim class.

        Args:
            a: First lattice vector. If float, a rectangular lattice is assumed: a = (a, 0.0).
                Any other iterable object is converted to tuple to be complient with S4 python
                interface.
            G: Integer, the number of plane waves in the Rayleigh expansion.
            b: Second lattice vector. Idem to a.
            sim_options: Dictionary with S4 simulation options.
                {"Verbosity" : 0,
                "LatticeTruncation" : 'Circular',

                "DiscretizedEpsilon" : False,
                "DiscretizationResolution" : 8,
                "PolarizationDecomposition" : True, ## FFT factorization rules
                "PolarizationBasis" : 'Default',
                "LanczosSmoothing" : False,
                "SubpixelSmoothing" : False,
                "ConserveMemory" : False}
            autostart: Boolean to determine when the calculation will start. If a simulation
                will be performed through the experiments modules, it should be set to False,
                an let the experiment methods to run the simulation.

        """

        if isinstance(a, float):
            self._a = np.r_[a, 0.0]

        if b is None:
            self._b = np.r_[0.0, 0.0]
        else:
            if isinstance(b, float):
                self._b = np.r_[0.0, b]

        self._center = np.r_[(self._a[0] + self._b[0]) / 2.0,
                             (self._a[1] + self._b[1]) / 2.0]
        self._G = G
        self.S = None
        self.unit_cell = {
            "Lattice": (tuple(self._a), tuple(self._b)),
            "NumBasis": self._G
        }
        self.sim_options = sim_options
        self.results = dict()  # dictionary of the results
        self.queries = set()  # queries
        self.sim_info = set(["unit_cell", "sim_options"])
        self.setters = collections.OrderedDict(
            (("new", None), ("materials", None), ("layers", None), ("excite",
                                                                    None)))
        # setters will define the autostart and restart functions,
        # child classes only need to add setters to reproduce the
        # simulation. It is an ordereddict as it is needed
        self.autostart = autostart

        if self.autostart:
            self._set_new()
        # if sim_options["Verbosity"] > 0 :
        # self.debug = True

    @deco_start
    def set_materials(self, materials=None, **kwargs):
        """Set Materials Function

        Args:
            materials (dictionary): Contains the  the label/name of the
                                    materials (string) and the refractive index
                                    (float/complex)

        Example:
            materials={"Vacuum":1.0, "SiO2":2.25}

        """
        self.materials = [{"Name": "Vacuum", "Epsilon": 1}]
        if materials is not None:
            # if type(materials) is dict:
            # for k, v in materials.iteritems():
            # self.materials.append({"Name":k,"Epsilon":v})
            # self.materials = materials
            # elif type(materials) is list:
            if isinstance(materials, (list, dict)):
                self.materials = materials
            else:
                raise TypeError('Material data not understood,\
                {}'.format(self.materials))
        self.sim_info.add("materials")

    @deco_start
    def set_layers(self, layers, **kwargs):
        """ Function to set the structure,
        Args:
            layers (list): List containing the dictionaries list

        Be careful as no check is done in kwargs
        for flexibility of inheritance, check siminfo for debug.
        The layers array of dictionaries, l0 is assumed always to be the
        first layer
        Example:
            layers = [{"Name": "l0", "Thickness":0., "Material":"Vacuum"},
                     {"Name": "l1", "Thickness":1., "Material":"Vacuum"},
                     {"Name": "l2", "Thickness":0., "Material":"Vacuum"}]
         """

        self.layers = layers
        self.total_thick = sum([layer["Thickness"] for layer in layers])
        self.sim_info.add("layers")

    @deco_start
    def set_excite(self,
                   l,
                   theta=0.,
                   phi=0.,
                   sAmplitude=1.,
                   pAmplitude=0,
                   **kwargs):
        """ Function to define the excitation planewave

        Args:
           l (float): wavelength [microns]
           theta (float): angle [degrees] 0-90
           phi (float): angle in [degrees] 0-360
           sAmplitude (complex): S-Polarization (Ey with phi=0)
           pAmplitude (complex): P-Polarization (Hy with phi=0)"""
        self.freq = 1. / l
        self.source = {
            "IncidenceAngles": (theta, phi),
            "sAmplitude": sAmplitude,
            "pAmplitude": pAmplitude,
            "Order": 0
        }

        self.sim_info.add("freq")
        self.sim_info.add("source")

    def start(self, run_calcs=False, **kwargs):
        """ Function to start the simulation object.

            The self.S object will be overwritten.

            Args:
            run_cals (bool, optional): If True it will run the calculus set in
                self.queries, like fluxes. These calculus must be
                properly set by its corresponding functions, for example
                get_fluxes with its monitors.
           """
        self._set_new()
        self.autostart = True
        for fun, dummy in self.setters.items():
            f = getattr(
                self,
                "_set_{}".format(fun))  # Call the S4 translated functions
            f()
        if run_calcs:
            self.run_calcs()

    def stop(self, clear_results=False, **kwargs):
        """Function to stop the interactive simulation

        it deletes the S4 object and optionally clear the results

        Args:
            clear_results (bool, optional): Clear previous results """
        self.autostart = False
        self.S = None
        if clear_results:
            self.clear_results()

    # def restart(self, ):

    def run_calcs(self, **kwargs):
        """Function to get the queries after modification of the
        structure for example"""

        for fun in self.queries:
            # print fun,self.results
            f = getattr(
                self,
                "_get_{}".format(fun))  # Rerun all the calcs with results#
            f()

    def empty(self, background="Vacuum", **kwargs):
        """ Function returns and equivalent empty structure

             The empty structure is useful to calculate the reflected field without interfernece
             of incident fieldi.
             Args:
                background (string, optional): The background material, Vacuum by default

        """
        S = self.S
        self.S = None
        result = copy(self)
        layers = list(self.layers)  # This way we do not modify the original
        # layers
        result.layers = layers
        for i, l in enumerate(self.layers):
            result.layers[i] = dict(l)
            result.layers[i]["Material"] = background
        self.S = S
        return result

    @deco_start
    def get_fluxes(self, monitors=None, **kwargs):
        """Function to get the Poynting Fluxes

        Args:
            Monitors (list): Monitors where to obtain the Poynting vector
        Returns:
            glist, fluxes (np.array) with indices glist[g_order],
            fluxes[layer_monitor, fordward/backward, order]

        Usage:
            glist, fluxes =  s1.get_fluxes(monitors=[{"Layer":"l0", "zOffset":0}])
        To obtain global flux just .sum()"""
        self.fluxes_monitors = self._z_monitor(monitors)
        self.sim_info.add("fluxes_monitors")
        self.queries.add('fluxes')

    @deco_start
    def get_amps(self, monitors=None, **kwargs):
        """ amplitudes by order (global flux just .sum()"""
        self.amps_monitors = self._z_monitor(monitors)
        self.sim_info.add("amps_monitors")

    @deco_start
    def get_fields_og(self, z=None, NPx=50, NPy=50, **kwargs):
        """Function to obtain Fields  at height z for a regular grid
           NPx, NPyx.
        Args:
            z (float, list, np.array): at a particular z coordinate.
            NPx, NPy (int): Size of the regular grid

        Note: Get fields using GetFieldsOnGrid, be careful, the
        phase for out of normal fields does not include the
        incident wave phase.
        """
        if z is None:
            z = np.linspace(-1., self.total_thick + 1.)
        if isinstance(z, float):
            z = [z]
        self.fields_og_monitors = [{
            "z": z_i,
            "NumSamples": (NPx, NPy),
            "Format": "Array"
        } for z_i in z]
        self.sim_info.add("fields_og_monitors")
        self.queries.add("fields_og")

    @deco_start
    def get_fields(self, x=0, y=0, z=0, **kwargs):
        """Function to get fields:
           Returns the eletric and magnetic fields for the coordinates x,y,z

           Args:
               x (float, list/tuple, numpy.array): List of postitions to get Fields
               y (float, list/tuple, numpy.array): List of postitions to get Fields
               x (float, list/tuple, numpy.array): List of postitions to get Fields

           Returns:
               np.array: containing the EH ([E,H]) fields with index [z, x, y, EH,
               EH_xyz]

           Examples:
                  >>>EH = grating.get_fields(0,0,[0,1])
                  >>>EH.shape
                  (2, 1, 1, 2, 3)
                  >>>E, H = EH[:,:,:,0,:], EH[:,:,:,1,:]

           Internally it uses GetFields, which is slower than GetFieldsOnGrid but  without
           any glitches for out of normal incidence

            """
        self.fields_monitors = []
        for u in [x, y, z]:
            if isinstance(u, (float, int)):  # Casting to list
                u = [u]
            self.fields_monitors.append(u)

        self.sim_info.add("fields_monitors")
        self.queries.add("fields")

    # def E(self):
    # """ Shortcut to obtain the Electric field after  """
    # if hasattr(self, 'fields_og'):
    # return self.fields_og[:,:,:,0,:]
    # else:
    # return self.fields[:,:,:,0,:]

    # def E(self):
    # """ Shortcut to obtain the Electric field """
    # if hasattr(self, 'fields_og'):
    # return self.fields_og[:,:,:,0,:]
    # else:
    # return self.fields[:,:,:,0,:]
    @deco_start
    def get_epsilon(self, x=0, y=0, z=0, **kwargs):
        """Function to get epsilon:
           Returns the epsilon used in the simulation  for the coordinates x,y,z

           Args:
               x (float, list/tuple, numpy.array): List of postitions to get Epsilon
               y (float, list/tuple, numpy.array): List of postitions to get Epsilon
               x (float, list/tuple, numpy.array): List of postitions to get Epsilon

           Returns:
               np.array: containing the epsilon np.array complex [z, x, y]
               """
        self.eps_monitor = []

        for u in [x, y, z]:
            if isinstance(u, (float, int)):  # Casting to list
                u = [u]
            self.eps_monitor.append(u)

        self.sim_info.add("eps_monitor")
        self.queries.add("epsilon")

    def _get_outputs(self, keys):
        return {key: getattr(self, key) for key in keys}

    def get_results(self):
        # return self._get_outputs(self.results)
        return self.results

    def get_info(self):
        return self._get_outputs(self.sim_info)

    def _saver(self, keys, savename, saveas, **kwargs):
        if saveas == 'hdf5':
            import hickle as hkl
            output_file = open('{}.hkl'.format(savename), 'w')
            compression = kwargs.get("compression", 'gzip')
            hkl.dump({key: getattr(self, key)
                      for key in keys},
                     output_file,
                     compression=compression)
            output_file.close()
            return 0
        elif saveas == 'dat':
            from pprint import pprint
            output_file = open('{}.dat'.format(savename), 'w')
            pprint({key: getattr(self, key)
                    for key in keys},
                   stream=output_file)
            output_file.close()
            return 0
        else:
            raise ValueError("Format not understood, (hdf5 or dat),\
                             not {}".format(saveas))

    def save_results(self,
                     savename,
                     saveas='hdf5',
                     compression='gzip',
                     **kwargs):
        """ Function to save the results

        Args:
            savename (str) : Save name, the extension will be set automatically.
            saveas (str, optional): filetype, "hdf5" or "dat" to store the data.
            compression (str, optional): only avaible for hdf5 typefile
            extra_args to be passsed directly to kwargs"""
        self._saver(
            self.results.keys(),  # old set structur, TODO use directly the dict
            "{}-results".format(savename),
            saveas=saveas,
            compression=compression)

    def save_siminfo(self, savename, saveas='dat'):
        """ Function to save the Simulation info

        Args:
            savename (str) : Save name, the extension will be set automatically.
            saveas (str, optional): filetype, "hdf5" or "dat" to store the data.
        """
        self._saver(self.sim_info, "{}-info".format(savename), saveas=saveas)

    def save_all(self, savename, **kwargs):
        """ Function to save the Simulation info (.dat) and results (.hdf5)

        Args:
            savename (str) : Save name, the extension will be set automatically.
            saveas (str, optional): filetype, "hdf5" or "dat" to store the data.
        """
        self.save_siminfo(savename)  # default for info dat
        self.save_results(savename)  # default for data hdf5 with compression
        return 0

    def clear_results(self, **kwargs):
        """ Function to clear the results of the simulation object:

            Namely it deletes the results as fluxes, or fields but
            structure, monitors, sim_info etc. are kept untouched"""
        for result in self.results.keys():
            setattr(self, result, None)
        results = dict()
        # self.results = set()

    def __copy__(self):
        cls = self.__class__
        result = cls.__new__(cls)
        result.__dict__.update(self.__dict__)
        return result

    def __deepcopy__(self, memo):
        cls = self.__class__
        result = cls.__new__(cls)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            if "S" != k:
                setattr(result, k, deepcopy(v, memo))
            else:
                # For the S4 Object use the internal clone
                if self.S is not None:
                    # setattr(result, k, self.S.Clone())
                    setattr(result, k, None)
        return result


class Patterned(Sim):
    def __init__(self,
                 a,
                 G=1,
                 b=None,
                 sim_options={"Verbosity": 0},
                 autostart=True,
                 **kwargs):
        """ a:  lattice parameter in microns
            b:  lattice parameter in microns
            G:  Numbasis used in the simulation
            Internal Options of the simulation
            options:    {"Verbosity" : 0,
            "LatticeTruncation" : 'Circular',
            "DiscretizedEpsilon" : False,
            "DiscretizationResolution" : 8,
            "PolarizationDecomposition" : True, ## FFT factorization rules
            "PolarizationBasis" : 'Default',
            "LanczosSmoothing" : False,
            "SubpixelSmoothing" : False,
            "ConserveMemory" : False}),
            autostart: Boolean
            """
        Sim.__init__(self,
                     a,
                     G=G,
                     b=b,
                     sim_options=sim_options,
                     autostart=autostart,
                     **kwargs)
        self.patterns = []

    @deco_start
    def set_patterns(self, patterns, **kwargs):
        """Function to add patterns [list] to the simulation, example of
        pattern:
            [{"Rectangle":
             {"Layer":"l1","Material":"Vacuum",
             "Center":(self._a/2.,0),"Angle":0.,
             "Halfwidths":(self._a/4.,0)}}]
        The layer must be the same in every list of patterns
        when doing holes."""
        # We have to found the layer used for the pattern for sort builts,
        # before holes.
        # layerP = patterns[0]
        # layerP = layerP[layerP.keys()[0]]["Layer"]
        # print layerP
        # lMaterial = [l for l in self.layers if l["Name"] == layerP][0]["Material"]
        # print(layerP)
        # print(lMaterial)
        # This sort doesn't work always
        # patterns = sorted(patterns,
        # key= lambda pattern: pattern[pattern.keys()[0]]["Material"] == lMaterial)
        # print(patterns)
        self.patterns.extend(patterns)
        self.sim_info.add("patterns")
        self.setters["patterns"] = None

    def set_structure(self, patterns=[], layers=None, **kwargs):
        """Deprecated, use set_layers, and set_pattern"""
        Sim.set_structure(self, layers=layers, **kwargs)
        # if patterns is None:
        # self.patterns = [{"Rectangle":
        # {"Layer":"l1","Material":"Vacuum",
        # "Center":(self._a/2.,0),"Angle":0.,
        # "Halfwidths":(self._a/4.,0)}}]
        # else:
        self.sim_info.add("patterns")
        self.setters["patterns"] = None
        self.patterns = patterns
        self.set_patterns()

    # def _set_patterns(self):
    # if self.patterns is not None:
    # for pattern in self.patterns:
    # pattype = pattern.keys()[0]
    # getattr(self.S, "SetRegion{}".format(pattype))(**pattern[pattype])
    # # [self.S.SetRegionRectangle(**pattern) for pattern in self.patterns]

    def empty(self, **kwargs):
        """ Function returns and equivalent empty structure, patterns are
        removed.
             The empty strucutre is useful to calculate the reflected field
             without interfernece of incident field
        Returns:
            empty_simulation (self class)
        """
        if len(self.patterns) > 0:
            # print("Cleaning patterns")
            empty_sim = self.empty_pattern()
            empty_sim = Sim.empty(empty_sim)
        else:
            empty_sim = Sim.empty(self)
        return empty_sim

    def empty_pattern(self, **kwargs):
        """ Function returns and equivalent structure without patterns, patterns are
        removed.

             The empty strucutre is useful to calculate the EM response
             without a nanostructure.
        Returns:
            empty_simulation (self class)
        """
        S = self.S
        self.S = None
        empty_sim = copy(self)
        self.S = S
        empty_sim.patterns = []
        # try:
        sim_info = set(self.sim_info)
        sim_info.remove("patterns")
        empty_sim.sim_info = sim_info
        # self.setters.remove("patterns")
        setters = collections.OrderedDict(empty_sim.setters)
        empty_sim.setters = setters
        try:
            del empty_sim.setters["patterns"]
        except KeyError:
            # print("No patterns to remove..")
            pass

        return empty_sim
