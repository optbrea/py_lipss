#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
nanoxy.py

This modules contains the classes to recreate generalized patterns in each
layer. The layers is recreated using a XY matrix :class: Matrix, or using a
:class: Polygon.
The 3D generalization (echa layer is defined by a Matrix or a Polygon are still
in development.
"""


import numpy as np

from . import simS4


def PolyArea(x, y):
    return 0.5 * np.abs(np.dot(x, np.roll(y, 1)) - np.dot(y, np.roll(x, 1)))


# TODO
# ADD TEMPLATES FOR EVERY CASE SO IT IS EASY TO GENERATE
# A STRUCTURE


class Matrix(simS4.Patterned):
    # def _add_pattern(self, patterns):
    # for pattern in patterns:
    # pattype = pattern.keys()[0]
    # getattr(self.S, "SetRegion{}".format(pattype))(**pattern[pattype])

    def add_pattern(self,
                    A,
                    mater_lut,
                    layer,
                    background="Vacuum",
                    pattern_type="Rectangle",
                    dN=None,
                    center_d=(0.0, 0.0)):
        """Function to add a matrix-2D pattern to a layer (square lattice)

        Args:
            A (np.array): x-y matrix.
            mater_lut (dict): Dictionary containing the material (keys
                string) and the values of the x-y matrix.
            layer (int): numbered layer to apply the pattern.
            background (string, optional): Material used as background of the
                layer, so any pixels with the value of the background would not
                generate a pattern.
            pattern_type (string): Pattern used for each pixel, can be Retangle
                or Circle.
            dN (float): size of the Pixel/Motive, by default the matrix use the
                full lattice so dN=a/(2 * A.shape), however smaller motives can be
                used.  There is no collision test, it would fail catastrophically.
            center_d (tuple): Allows to displaced the center of each motive by
                a vector.


        TODO:
         Generalize for non-square matrix.
        """
        #A = DOE[DOE_TYPE]
        A = A / A.max()
        a = self._a[0]
        if dN is None:
            dN = a / (2. * A.shape[0])

        a_pix = (a / 2 - dN)
        X, Y = np.meshgrid(
            np.linspace(-a_pix, a_pix, A.shape[0]),
            np.linspace(-a_pix, a_pix, A.shape[0]))
        coord = np.array([[x, y] for x, y in zip(X.flatten(), Y.flatten())])
        patterns = []
        if type(dN) is float:
            DN = np.full_like(A, dN)
        CTD = np.zeros((2, A.shape[0], A.shape[1]))
        for i, ctd in enumerate(center_d):
            if type(center_d[0]) is float:
                CTD[i, :, :] = np.full_like(A, center_d[0])
            else:
                CTD[i, :, :] = ctd

        # for k,v in mater_lut.iteritems():
        # if k != background:

        if isinstance(mater_lut.keys()[0], str):
            mater_lut2 = {v: k for k, v in mater_lut.iteritems()}
            mater_lut = mater_lut2

        for xy, lut, dn, ctdx, ctdy in zip(coord, A.flatten(), DN.flatten(),
                                           CTD[0, :, :].flatten(),
                                           CTD[1, :, :].flatten()):
            if mater_lut[int(lut)] != background:
                # print xy, np.array(ctdx, ctdy)
                pattern = {
                    pattern_type: {
                        "Layer": self.layers[layer]["Name"],
                        "Center": tuple(xy + np.array((ctdx, ctdy))),
                        "Material": mater_lut[int(lut)],
                    }
                }
                if pattern_type == "Rectangle":
                    pattern[pattern_type]["Halfwidths"] = (dn, dn)
                    pattern[pattern_type]["Angle"] = 0
                elif pattern_type == "Circle":
                    pattern[pattern_type]["Radius"] = dn
                patterns.append(pattern)
                # print pattern

        if len(patterns) > 0:
            self.set_patterns(patterns)
            # self.setters["patterns"]=None
            # if self.patterns is None:
        # # self.patterns = []
        # self.patterns = patterns
        # self.sim_info.add("patterns")
        # else:
        # self.patterns.extend(patterns)

        else:
            pass
        # if self.autostart:
        # self._set_patterns()


class Polygon(simS4.Patterned):

    # def _add_pattern(self, patterns):
    # for pattern in patterns:
    # pattype = pattern.keys()[0]
    # getattr(self.S, "SetRegion{}".format(pattype))(**pattern[pattype])

    def add_pattern(self, poly_dict, mater_lut, layer, **kwargs):
        """Function to add a poly-2D pattern to a layer

        Args:
            poly_dict: polygon dictionary #TODO ADD TO TEMPLATE
            mater_lut (dict): Dictionary containing the material (keys
                string) and the values of the x-y matrix.
            layer (int): numbered layer to apply the pattern.

        """
        patterns = []
        # First we have to sort the Polygons to avoid Allocation ERROR
        areas = [
            PolyArea(patt[:, 0], patt[:, 1]) for patt in poly_dict['CONTOURS']
        ]
        areas = np.array(areas)
        order = areas.argsort()
        order = order[::-1]  # We put the bigger ones first

        poly_dict['CONTOURS'] = [
            poly_dict['CONTOURS'][i] for i in order if areas[i] > 0
        ]
        poly_dict['MATERIALS'] = [
            poly_dict['MATERIALS'][i] for i in order if areas[i] > 0
        ]

        # Reorganize the material lut to look by int values not name
        if isinstance(mater_lut.keys()[0], str):
            mater_lut2 = {v: k for k, v in mater_lut.iteritems()}
            mater_lut = mater_lut2

        for k, v in mater_lut.iteritems():
            for contour, material in zip(poly_dict['CONTOURS'],
                                         poly_dict['MATERIALS']):
                # print contour, material
                vertices = []
                for x, y in contour:
                    vertices.append((x * self._a, y * self._a))
                vertices.pop()
                # Remove lines
                # if len(vertices)<=2:
                # print "Remove line, artifact"
                # else:
                poly = {
                    "Polygon": {
                        'Layer': self.layers[layer]["Name"],
                        'Vertices': tuple(vertices),
                        'Center': (0, 0),
                        'Material': mater_lut[int(material)],
                        'Angle': 0,
                    }
                }
                patterns.append(poly)

        if len(patterns) > 0:
            self.set_patterns(patterns)
            # if self.patterns is None:
        # # self.patterns = []
        # self.patterns = patterns
        # self.sim_info.add("patterns")
        # else:
        # self.patterns.extend(patterns)

        else:
            print("No pattern added")
        # if self.autostart:
        # self._set_patterns()


class Matrix3D(Matrix):
    def add_pattern(self,
                    As,
                    layers,
                    mater_lut,
                    background="Vacuum",
                    pattern_type="Rectangle",
                    dN=None,
                    center_d=(0, 0)):
        """Function to add a matrix-3D pattern to a list of layers
        (NEED TESTING)

        Args:
            As (np.array): x-y-z matrix.
            mater_lut (dict): Dictionary containing the material (keys
                string) and the values of the x-y matrix.
            layer (list): numbered layers to apply the pattern.
            background (string, optional): Material used as background of the
                layer, so any pixels with the value of the background would not
                generate a pattern.
            pattern_type (string): Pattern used for each pixel, can be Retangle
                or Circle.
            dN (float): size of the Pixel/Motive, by default the matrix use the
                full lattice so dN=a/(2 * A.shape), however smaller motives can be
                used.  There is no collision test, it would fail catastrophically.
                TODO: GENERALIZE dN for 3D
            center_d (tuple): Allows to displaced the center of each motive by
                a vector.
        """
        # TODO center displacement and dN in 3D
        for i, layer in enumerate(layers):
            # if dN is None:
            # dN = self._a / (2. * As[:,i].shape[0])
            # elif type(dN) is float:
            # DN = np.full_like(As[:,i], dN)
            # else:
            # DN = dN
            Matrix.add_pattern(
                As[:, i],
                mater_lut,
                layer,
                background=background,
                pattern_type=pattern_type,
                dN=dN,
                center_d=center_d)


class Poly3D(Polygon):
    def add_pattern(self, poly_dicts, mater_lut, layers, **kwargs):
        """Function to add poly-2D pattern to list of layer

        Args:
            poly_dicts: polygon dictionary list #TODO ADD TO TEMPLATE
            mater_lut (dict): Dictionary containing the material (keys
                string) and the values of the x-y matrix.
            layer (lits): numbered layers to apply the pattern.

        """
        for i, layer in enumerate(layers):
            Polygon.add_pattern(poly_dicts[i], mater_lut, layer)
