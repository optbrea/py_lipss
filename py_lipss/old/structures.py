#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
This module is to generate standard structures

"""

um = 1.


def planar_structure(materials_names, thicknesses):

    layers = []

    for i in range(len(thicknesses)):
        layer_name = "l{}".format(i)
        layer = dict(
            Name=layer_name,
            Material=materials_names[i],
            Thickness=thicknesses[i])

        layers.append(layer)

    patterns = []
    return layers, patterns


def multilayer(ncs, depths, material_names):
    """Generates the the structure of a multilayer.


    Parameters:
        ncs: array with refraction index
        names: array with names
        depths: array with depths
        material_names: array with materials:

    Example of parameters:
        ncs=[1, 1.5+0.01j, 2.25, 1]
        material_names= ["Vaccum",  "material_1", "material_2", "Vacuum"]
        depths=[0,2, 2,0]

    """
    layers = []

    for i, name in enumerate(material_names):
        dictionary = {}
        dictionary["Name"] = "l" + str(i)
        dictionary["Thickness"] = depths[i]
        dictionary["Material"] = material_names[i]
        layers.append(dictionary)

    return layers


def binary_grating(materials_names, period, height, fill_factor):
    layers = [
        {
            "Name": "l0",
            "Thickness": 0.,
            "Material": materials_names[0]
        },
        {
            "Name": "l1",
            "Thickness": height,
            "Material": materials_names[0]
        },
        {
            "Name": "l2",
            "Thickness": 0.,
            "Material": materials_names[1]
        },
    ]

    patterns = [{
        "Rectangle": {
            "Layer": "l1",
            "Material": materials_names[1],
            "Center": (0, 0),
            "Angle": 0.,
            "Halfwidths": (period * fill_factor / 2., 0)
        }
    }]

    return layers, patterns
