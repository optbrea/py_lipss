#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np


def get_E_0D(grt, no_grating, no_cell, kind='reflected'):
    """Get E and H returning 1D fields. This is useful for polation in subwavelength structures (using get_stokes_from_E)

    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        E_0D (np.array): 1x3 E field

    Warning:
        TOTEST
    """
    if kind == 'reflected':
        E = grt.fields[:, 0, 0, 0] - no_cell.fields[:, 0, 0, 0]
    elif kind == 'transmitted':
        E = grt.fields[:, 0, 0, 0]

    E_0D = E.sum(axis=0)

    return E_0D


def get_E_1D(grt, no_grating, no_cell, kind='reflected'):
    """Get E and H returning 1D fields. This is useful for polation in subwavelength structures (using get_stokes_from_E)

    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        E_0D (np.array): 1x3 E field

    Warning:
        TOTEST
    """
    if kind == 'reflected':
        E = grt.fields[:, 0, 0, 0] - no_cell.fields[:, 0, 0, 0]
    elif kind == 'transmitted':
        E = grt.fields[:, 0, 0, 0]
    return E


def get_EH(fields, xyz=None, has_draw=False):
    """Determines the fields from the fields parameter
    """
    def draw_field(E, x_f, z_f):

        extent = [x_f[0], x_f[-1], z_f[-1], z_f[0]]
        plt.imshow(E, extent=extent)
        plt.colorbar()

    Ex = np.squeeze(fields[:, :, :, 0, 0])
    Ey = np.squeeze(fields[:, :, :, 0, 1])
    Ez = np.squeeze(fields[:, :, :, 0, 2])

    Hx = np.squeeze(fields[:, :, :, 1, 0])
    Hy = np.squeeze(fields[:, :, :, 1, 1])
    Hz = np.squeeze(fields[:, :, :, 1, 2])

    S_max = np.real(
        np.max((Ex.real, Ey.real, Ez.real, Hx.real, Hy.real, Hz.real)))
    S_min = np.real(
        np.min((Ex.real, Ey.real, Ez.real, Hx.real, Hy.real, Hz.real)))

    if has_draw:
        dims = np.shape(Ex)
        num_dims = len(dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])

            plt.figure()
            plt.subplot(3, 2, 1)
            plt.plot(z0, np.real(Ex))
            plt.title("$E_x$")
            plt.ylim(S_min, S_max)

            plt.subplot(3, 2, 3)
            plt.plot(z0, np.real(Ey))
            plt.title("$E_y$")
            plt.ylim(S_min, S_max)

            plt.subplot(3, 2, 5)
            plt.plot(z0, np.real(Ez))
            plt.title("$E_z$")
            plt.ylim(S_min, S_max)

            plt.subplot(3, 2, 2)
            plt.plot(z0, np.real(Hx))
            plt.title("$H_x$")
            plt.ylim(S_min, S_max)

            plt.subplot(3, 2, 4)
            plt.plot(z0, np.real(Hy))
            plt.title("$H_y$")
            plt.ylim(S_min, S_max)

            plt.subplot(3, 2, 6)
            plt.plot(z0, np.real(Hz))
            plt.title("$H_z$")
            plt.ylim(S_min, S_max)

            plt.suptitle("Average Pointing vector")

        elif num_dims == 2:
            x0, z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[1])
            if x0 is None:
                x0 = np.linspace(0, 1, dims[0])

            plt.suptitle("Fields")

            plt.figure()
            plt.subplot(3, 2, 1)
            draw_field(np.real(Ex), x0, z0)
            plt.title("$E_x$")
            plt.clim(S_min, S_max)

            plt.subplot(3, 2, 3)
            draw_field(np.real(Ey), x0, z0)
            plt.title("$E_y$")
            plt.clim(S_min, S_max)

            plt.subplot(3, 2, 5)
            draw_field(np.real(Ez), x0, z0)
            plt.title("$E_z$")
            plt.clim(S_min, S_max)

            plt.subplot(3, 2, 2)
            draw_field(np.real(Hx), x0, z0)
            plt.title("$H_x$")
            plt.clim(S_min, S_max)

            plt.subplot(3, 2, 4)
            draw_field(np.real(Hy), x0, z0)
            plt.title("$H_y$")
            plt.clim(S_min, S_max)

            plt.subplot(3, 2, 6)
            draw_field(np.real(Hz), x0, z0)
            plt.title("$H_z$")
            plt.clim(S_min, S_max)

    return Ex, Ey, Ez, Hx, Hy, Hz


def draw_EH(Ex, Ey, Ez, Hx, Hy, Hz, xyz=None):
    """Determines the fields from the fields parameter.
    With this function, you can remove incident field
    """

    S_max = np.real(np.max((Ex, Ey, Ez, Hx, Hy, Hz)))
    S_min = np.real(np.min((Ex, Ey, Ez, Hx, Hy, Hz)))

    dims = np.shape(Ex)
    num_dims = len(dims)
    if num_dims == 1:
        z0 = xyz
        if z0 is None:
            z0 = np.linspace(0, 1, dims[0])

        plt.figure()
        plt.subplot(3, 2, 1)
        plt.plot(z0, np.real(Ex))
        plt.title("$E_x$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 3)
        plt.plot(z0, np.real(Ey))
        plt.title("$E_y$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 5)
        plt.plot(z0, np.real(Ez))
        plt.title("$E_z$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 2)
        plt.plot(z0, np.real(Hx))
        plt.title("$H_x$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 4)
        plt.plot(z0, np.real(Hy))
        plt.title("$H_y$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 6)
        plt.plot(z0, np.real(Hz))
        plt.title("$H_z$")
        plt.ylim(S_min, S_max)

        plt.suptitle("Average Pointing vector")
    elif num_dims == 2:
        x0, z0 = xyz
        if z0 is None:
            z0 = np.linspace(0, 1, dims[1])
        if x0 is None:
            x0 = np.linspace(0, 1, dims[0])

        plt.suptitle("Fields")

        plt.figure()
        plt.subplot(3, 2, 1)
        plt.imshow(np.real(Ex))
        plt.title("$E_x$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 3)
        plt.imshow(np.real(Ey))
        plt.title("$E_y$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 5)
        plt.imshow(np.real(Ez))
        plt.title("$E_z$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 2)
        plt.imshow(np.real(Hx))
        plt.title("$H_x$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 4)
        plt.imshow(np.real(Hy))
        plt.title("$H_y$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 6)
        plt.imshow(np.real(Hz))
        plt.title("$H_z$")
        plt.clim(S_min, S_max)


def get_EH_2D(sim, z=None, fields_og=False, **kwargs):
    """Get E and H returning 3D fields

    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        E (np.array): *x* E field
        H (np.array): *x* H field

    Warning:
        TOTEST
    """

    if fields_og:
        fields = sim.fields_og
        E = fields[:, 0, :, 0, :]
        H = fields[:, 1, :, 0, :]
    else:
        fields = sim.fields
        E = fields[:, :, 0, 0, :]
        H = fields[:, :, 0, 1, :]
    if z is None:
        z = sim.fields_monitors[2]
    E = np.linalg.norm(E, axis=-1)
    H = np.linalg.norm(H, axis=-1)

    return E, H


def get_EH_3D(sim, z=None):
    """Get E and H returning 3D fields

    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        E (np.array): *x* E field
        H (np.array): *x* H field

    Warning:
        it had z, fields_og, **kwargs, but were not used

    Warning:
        TOTEST
    """
    Ex = sim.results['fields'][:, :, 0, 0, 0].squeeze()
    Ey = sim.results['fields'][:, :, 0, 0, 1].squeeze()
    Ez = sim.results['fields'][:, :, 0, 0, 2].squeeze()

    Hx = sim.results['fields'][:, :, 0, 1, 0].squeeze()
    Hy = sim.results['fields'][:, :, 0, 1, 1].squeeze()
    Hz = sim.results['fields'][:, :, 0, 1, 2].squeeze()

    d1, d2 = np.shape(Ex)

    E = np.zeros((d1, d2, 3), dtype=complex)
    H = np.zeros((d1, d2, 3), dtype=complex)
    E[:, :, 0] = Ex
    E[:, :, 1] = Ey
    E[:, :, 2] = Ez
    H[:, :, 0] = Hx
    H[:, :, 1] = Hy
    H[:, :, 2] = Hz

    return E, H


def get_eps(sim):
    """Determines the dielectric constant in terms of position


    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        eps_real (np.array): real part of dielectric constant
        eps_imag (np.array). imaginary part of dielectric constant

    Warning:
        it had z, fields_og, **kwargs, but were not used

    Warning:
        TOTEST
    """

    eps = sim.eps[:, :, 0]
    eps_real = np.real(eps)
    eps_imag = np.imag(eps)

    return eps_real, eps_imag


def get_refraction_index(sim):
    """Determines the refraction index in terms of position


    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        n_real (np.array): real part of refraction index
        n_imag (np.array). imaginary part refraction index

    Warning:
        it had z, fields_og, **kwargs, but were not used

    Warning:
        TOTEST, TODO
    """

    eps = sim.eps[:, :, 0]
    eps_real = np.real(eps)
    eps_imag = np.imag(eps)

    n_real = np.ones_like(eps_real)
    n_imag = np.zeros_like(eps_imag)

    return n_real, n_imag


def get_diffraction_orders(sim):
    """Determines the refraction index in terms of position


    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        n_real (np.array): real part of refraction index
        n_imag (np.array). imaginary part refraction index

    Warning:
        it had z, fields_og, **kwargs, but were not used

    Warning:
        TOTEST, TODO
    """
    glist = sim.glist
    fluxes_out = sim.fluxes

    efficiency_reflectance = -fluxes_out[0, :, 1]
    efficiency_transmitance = -fluxes_out[1, :, 1]

    glist = glist[:, 0]

    return glist, efficiency_reflectance, efficiency_transmitance


def get_amplitudes(sim):
    """Determines the amplitudes of the reflected field?


    Parameters:
        sim (simS4.Patterned): structure defined and with data

    Returns:
        amplitudes (np.array): Reflected amplitudes of the field

    Warning:
        TOTEST, TODO
    """
    glist = sim.glist
    amps_out = sim.amps
    G = glist.shape[0]

    amplitudes = -amps_out[0, 1, :G]

    return amplitudes


# Reflectance
def get_efficiency_reflectance(exp, exp0=None, verbose=True):

    R = -exp.fluxes[0, :, 1].sum().real

    if exp0 is not None:
        R0 = -exp0.fluxes[0, :, 1].sum().real
        efficiency = R / R0

    else:
        R0 = None
        efficiency = None

    if verbose is True:
        print("Output R={0:.2f}, R0={0:.2f}, eff={1:.2f}%".format(
            R, R0, efficiency))

    return R, R0, efficiency


# Transmitance
def get_efficiency_transmitance(exp, exp0=None, verbose=True):
    T = exp.fluxes[2, :, 0].sum().real

    if exp0 is not None:
        T0 = exp0.fluxes[2, :, 0].sum().real
        efficiency = T / T0

    else:
        T0 = None
        efficiency = None

    if verbose is True:
        print("Output T={0:.2f}, T0={0:.2f}, eff={1:.2f}%".format(
            T, T0, efficiency))

    return T, T0, efficiency


def efficiencies(exp, exp0='', verbose=True):
    R, R0, eff_R = get_efficiency_reflectance(exp, exp0, verbose)
    T, T0, eff_T = get_efficiency_transmitance(exp, exp0, verbose)

    if verbose is True:
        print("____________________________________")
        print("reflectance  = {:2.4f}".format(R))
        print("transmitance = {:2.4f}".format(T))
        print("total        = {:2.4f}\n".format(R + T))
        print("efficiencies: R = {:2.4f} T = {:2.4f}".format(eff_R, eff_T))

    return R, T, R0, T0, eff_R, eff_T


def Poynting_vector(Ex, Ey, Ez, Hx, Hy, Hz, xyz=None, has_draw=True):

    "Instantaneous Poynting Vector"

    def draw_field(E, x_f, z_f):

        extent = [x_f[0], x_f[-1], z_f[-1], z_f[0]]
        plt.imshow(E, extent=extent)
        plt.colorbar()

    Sx = Ey * Hz - Ez * Hy
    Sy = Ez * Hx - Ex * Hz
    Sz = Ex * Hy - Ey * Hx

    S_max = np.real(np.max((Sx, Sy, Sz)))
    S_min = np.real(np.min((Sx, Sy, Sz)))
    S_lim = np.max((abs(S_max), np.abs(S_min)))

    if has_draw:
        dims = np.shape(Ex)
        num_dims = len(dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(3, 1, 1)
            plt.plot(z0, np.real(Sx))
            plt.ylim(-S_lim, S_lim)
            plt.title("$S_x$")

            plt.subplot(3, 1, 2)
            plt.plot(z0, np.real(Sy))
            plt.ylim(-S_lim, S_lim)
            plt.title("$S_y$")

            plt.subplot(3, 1, 3)
            plt.plot(z0, np.real(Sz))
            plt.ylim(-S_lim, S_lim)
            plt.title("$S_z$")

        elif num_dims == 2:
            x0, z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[1])
            if x0 is None:
                x0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(1, 3, 1)
            plt.title("$S_x$")
            draw_field(np.real(Sx), x0, z0)
            plt.clim(-S_lim, S_lim)

            plt.subplot(1, 3, 2)
            plt.title("$S_y$")
            draw_field(np.real(Sy), x0, z0)
            plt.clim(-S_lim, S_lim)

            plt.subplot(1, 3, 3)
            draw_field(np.real(Sz), x0, z0)
            plt.clim(-S_lim, S_lim)
            plt.title("$S_z$")

        plt.suptitle("Instantaneous Pointing vector")

    return Sx, Sy, Sz


def Poynting_vector_averaged(Ex, Ey, Ez, Hx, Hy, Hz, xyz=None, has_draw=False):

    "Averaged Poynting Vector"

    def draw_field(E, x_f, z_f):

        extent = [x_f[0], x_f[-1], z_f[-1], z_f[0]]
        plt.imshow(E, extent=extent)
        plt.colorbar()

    Sx = np.real(Ey * Hz.conjugate() - Ez * Hy.conjugate())
    Sy = np.real(Ez * Hx.conjugate() - Ex * Hz.conjugate())
    Sz = np.real(Ex * Hy.conjugate() - Ey * Hx.conjugate())

    S_max = np.real(np.max((Sx, Sy, Sz)))
    S_min = np.real(np.min((Sx, Sy, Sz)))
    S_lim = np.max((abs(S_max), np.abs(S_min)))

    if has_draw:
        dims = np.shape(Ex)
        num_dims = len(dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(3, 1, 1)
            plt.plot(z0, Sx)
            plt.ylim(-S_lim, S_lim)
            plt.title("$S_x$")

            plt.subplot(3, 1, 2)
            plt.plot(z0, Sy)
            plt.title("$S_y$")
            plt.ylim(-S_lim, S_lim)

            plt.subplot(3, 1, 3)
            plt.plot(z0, Sz)
            plt.title("$S_z$")
            plt.ylim(-S_lim, S_lim)

            plt.suptitle("Average Pointing vector")
        elif num_dims == 2:
            x0, z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[1])
            if x0 is None:
                x0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(1, 3, 1)
            plt.title("$S_x$")
            draw_field(np.real(Sx), x0, z0)
            plt.clim(-S_lim, S_lim)

            plt.subplot(1, 3, 2)
            plt.title("$S_y$")
            draw_field(np.real(Sy), x0, z0)
            plt.clim(-S_lim, S_lim)

            plt.subplot(1, 3, 3)
            draw_field(np.real(Sz), x0, z0)
            plt.title("$S_z$")
            plt.clim(-S_lim, S_lim)

            plt.suptitle("Average Pointing vector")
            plt.tight_layout()

    return Sx, Sy, Sz


def Poynting_total(Ex, Ey, Ez, Hx, Hy, Hz, xyz=None, has_draw=False):
    Sx, Sy, Sz = Poynting_vector_averaged(Ex,
                                          Ey,
                                          Ez,
                                          Hx,
                                          Hy,
                                          Hz,
                                          xyz=xyz,
                                          has_draw=False)

    S = np.abs(Sx + Sy + Sz)

    plt.figure()
    plt.subplot(1, 1, 1)
    plt.title("$S_x+S_y+S_z$")
    plt.imshow(S)
    plt.colorbar(orientation='vertical')
    plt.set_cmap('hot')


def energy_density(Ex, Ey, Ez, Hx, Hy, Hz, epsilon, xyz=None, has_draw=False):

    U = epsilon * (np.abs(Ex)**2 + np.abs(Ey)**2 + np.abs(Ez)**2) + (
        np.abs(Hx)**2 + np.abs(Hy)**2 + np.abs(Hz)**2)

    if has_draw:
        dims = np.shape(Ex)
        num_dims = len(dims)
        print(num_dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(1, 1, 1)
            plt.plot(z0, np.real(U))

        elif num_dims == 2:
            x0, z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[1])
            if x0 is None:
                x0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(1, 1, 1)
            plt.imshow(np.real(U))
            plt.colorbar()
            plt.clim(vmin=0)

        plt.title("energy_density")
    return U
