#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import copy
import os

import numpy as np


class Experiment(object):
    def __init__(self, nano, pols=((0, 1)), GN=None, **kwargs):
        self.exps = []
        self._empty_exp = []  # class that copy nano but stop adding patterns
        self.pols = pols
        for pol in pols:
            nanon = copy.deepcopy(nano)
            nanon.source["pAmplitude"] = pol[0]
            nanon.source["sAmplitude"] = pol[1]
            if GN is not None:
                nanon.unit_cell["NumBasis"] = GN
            nanoe = copy.deepcopy(nanon)
            nanoe.unit_cell["NumBasis"] = 1
            # print nanoe.source
            # print nanon.source
            nanoe.empty(rerun=False)
            self.exps.append(nanon)
            self._empty_exp.append(nanoe)

    def run_experiment(self, **kwargs):
        for nanon, nanoe in zip(self.exps, self._empty_exp):
            nanon.restart_sim(rerun=False)
            nanoe.restart_sim(rerun=False)
            nanon.get_again()
            nanoe.get_again()

    def get_stokes_from_E(self, E, **kwargs):
        """Function to obtain the Stokes parameter for an array E (Ex, Ey)

        Parameters:
            E (np.array): Input field
        Returns:
            SS (np.array): Stokes parameters (IQUV)
        """
        Ex, Ey = E[0], E[1]
        Exp = Ex / np.sqrt(np.conjugate(Ex) * Ex + np.conjugate(Ey) * Ey)
        Eyp = Ey / np.sqrt(np.conjugate(Ex) * Ex + np.conjugate(Ey) * Ey)
        Ex = Exp
        Ey = Eyp
        SS = [0, 0, 0, 0]
        SS[0] = np.conjugate(Ex) * Ex + np.conjugate(Ey) * Ey
        SS[1] = np.conjugate(Ex) * Ex - np.conjugate(Ey) * Ey
        SS[2] = 2 * (Ex * np.conjugate(Ey)).real
        SS[3] = 2 * (Ex * np.conjugate(Ey)).imag
        SS = np.array(SS)
        return SS

    def get_stokes_z(self, z, **kwargs):
        stk = []
        for exp, exp0 in zip(self.exps, self._empty_exp):
            EH1 = np.array(exp.S.GetFields(0, 0, z))
            EH0 = np.array(exp0.S.GetFields(0, 0, z))
            E = EH1[0, :] - EH0[0, :]
            stk.append(self.get_stokes_from_E(E))
        return np.array(stk)

    def get_analysis(self, upx, upy, upz, method="stokes"):
        self.analysis = {}
        self.analysis[method] = [None] * len(self.exps)
        # stokes_parameters = []
        for i, exp in enumerate(self.exps):
            # use the i to avoid copying again all the experiment values
            E1 = exp.fields[upx, upy, upz, 0, :]
            E0 = self._empty_exp[i].fields[upx, upy, upz, 0, :]
            E = E1 - E0
            self.analysis[method][i] = getattr(self, method)(E)

    def line_reflected_field(self, z, **kwargs):
        for exp, exp0 in zip(self.exps, self._empty_exp):
            EH1 = np.array([exp.S.GetFields(0, 0, zi) for zi in z])
            EH0 = np.array([exp0.S.GetFields(0, 0, zi) for zi in z])
            E = EH1[:, 0, :] - EH0[:, 0, :]
            exp.E_lz = E
            exp0.E_lz = EH0[:, 0, :]

    def get_jones_2d(self, z, source=False, **kwargs):
        self.line_reflected_field(z)
        J1s = []
        for exp, exp0 in zip(self.exps, self._empty_exp):
            J1 = exp.E_lz.sum(axis=-2)[:2]
            J1 = J1 - min(abs(J1[0].imag), abs(J1[1].imag)) * 1j
            J1 = J1 / np.sqrt(np.dot(np.conjugate(J1), J1))
            exp.jones = J1
            if source:
                J0 = exp0.E_lz.sum(axis=-2)[:2]
                J0 = J0 - min(abs(J0[0].imag), abs(J0[1].imag)) * 1j
                J0 = J0 / np.sqrt(np.dot(np.conjugate(J0), J0))
                exp0.jones = J0
            J1s.append(J1)
        return np.array(J1s)

    def save_results(self, savename):
        os.system('mkdir -p {}'.format(savename))
        for pols, exp in zip(self.pols, self.exps):
            saveloc = '{}/(S={}, P={})'.format(savename, *self.pols)
            os.system('mkdir -p {}'.format(saveloc))
            exp.save_all('{}/{}'.format(saveloc, 'output'))
