#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import numpy as np


def jumps(a, v, *args, **kwargs):
    """ Function to return the jumps in a True-False array:
    a: int value of the mask
    v: the array """
    lut = a == v
    dZ = lut[1:] ^ lut[:-1]
    j, = np.where(dZ)
    return j
