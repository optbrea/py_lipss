#!/usr/bin/env python3
# -*- coding: utf-8 -*-


def compute_3_structures(grating):
    """Function to calculate the results stored in the grating pyS4 class for the nanoestructure, the planar and the empty. These two last structures are required form comparison. For example, to remove the incident field in polarization computations and to compare the efficiency of the nanoestructure comparison with the plane interaface.

    Parameters:
        grating (simS4.Patterned)

    Returns:
        grating (simS4.Patterned:      fields with the nanoestructure
        no_grating (simS4.Patterned):  field without the nanoestructure, just the plane surfaces.
        no_cell (simS4.Patterned):     field at vaccum, with no material.

    """
    grating.stop()
    no_cell = grating.empty()
    no_grating = grating.empty_pattern()

    grating.start(run_calcs=True)
    no_grating.start(run_calcs=True)
    no_cell.start(run_calcs=True)

    return grating, no_grating, no_cell


def compute_field_empty_structure(grating):
    """Function to calculate the results stored in the grating pyS4 class for the nanoestructure, the planar and the empty. These two last structures are required form comparison. For example, to remove the incident field in polarization computations and to compare the efficiency of the nanoestructure comparison with the plane interaface.

    Parameters:
        grating (simS4.Patterned)

    Returns:
        grating (simS4.Patterned:   fields with the nanoestructure
        no_grating (simS4.Patterned):  fiedl without the nanoestructure, just the plane surfaces.
        no_cell (simS4.Patterned):     field at vaccum, with no material.

    """
    grating.stop()
    no_cell = grating.empty()

    # grating.start(run_calcs=True)
    no_cell.start(run_calcs=True)

    return no_cell
