#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
import scipy as sp

um = 1.
"""
INFO: function names changed
    drawing_cos -> LUT_cos
    drawing_cos2 -> LUT_cos2
    drawing_zxfun -> LUT_zxfun
    drawing_halfcos -> LUT_halfcos
    drawing_triangle -> LUT_triangle
    drawing_cricle -> LUT_circle
"""


def LUT_xzfun(x0,
              z0,
              NL,
              A=1.,
              xres=10,
              offset=0,
              kind='linear',
              has_draw=False):
    """ Function to generate a 2d image, z = f(x). Between the maximum
    and minium value the function values are set
    to True, and upper to False. The minimum value in z will be the floor of the
    function, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis.
    Args:
        x0 (np.array): x-direction points
        z0 (np.array): z-direction points
        NL (int): Number of layers (discretization in height, z)
        A (float, 0-1): Normalized Amplitude of the function
        xres (int >=1) : Discretization in x
        zoffset(float): Offset in z direction of the function.
    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (NL, xres*NL)
    """
    x, y = np.linspace(x0.min(), x0.max(), NL * xres), np.linspace(0, 1., NL)
    X, Y = np.meshgrid(x, y)
    z0 = -z0
    z0 = (z0 - z0.min())
    z0 = z0 / z0.max()
    F = sp.interpolate.interp1d(x0, z0, kind=kind, bounds_error=None)
    LUT = Y - F(X) + offset >= 0

    if has_draw is True:
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.imshow(LUT)
        plt.subplot(2, 1, 2)
        plt.plot(x0, 1 - z0)
        plt.xlim(x0[0], x0[-1])

    return LUT


def LUT_cos(NL, P, A=1., xres=10, offset=0, has_draw=False):
    """ Function to generate a cos/sin 2d image, z = cos(x). Under the cosine values are set
    to True, and upper to False, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis. This is useful
    for RCWA .
    Args:
        NL (int): Number of layers (discretization in height, z)
        P (float, 0-1): Normalized period of the cos function
        A (float, 0-1): Normalized Amplitude of the cos function
        xres (int >=1) : Discretization in x
        zoffset(float): Offset in z direction of the cos function.
        has_draw (bool): If True, draws shape
    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (NL, xres*NL)
    """
    x = np.linspace(-0.5, 0.5, NL * xres)
    y = np.linspace(-1., 1., NL)
    X, Y = np.meshgrid(x, y)

    func = A * np.cos(2. * X * np.pi / P - np.pi) + (1. - A) + offset * 2.
    LUT = Y - func >= 0
    LUT[abs(X) > P / 2.] = False  # Only one structure within the unit-cell

    if has_draw is True:
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.imshow(LUT)
        plt.subplot(2, 1, 2)
        plt.plot(x, func[0, :])

    return LUT


def LUT_cos2(NL, P, A=1, xres=10, fill_factor=1, has_draw=False):
    """ Function to generate a cos/sin 2d image, z = cos(x). Under the cosine values are set
    to True, and upper to False, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis. This is useful
    for RCWA .
    Args:
        NL (int): Number of layers (discretization in height, z)
        P (float, 0-1): Normalized period of the cos function
        A (float, 0-1): Normalized Amplitude of the cos function
        xres (int >=1) : Discretization in x
        has_draw (bool): If True, draws shape

    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (NL, xres*NL)
    """
    x = np.linspace(-0.5, 0.5, NL * xres)
    y = np.linspace(-1., 1., NL)
    X, Y = np.meshgrid(x, y)

    func = A * ((1 - 2 * (
        (1 + np.cos(2 * np.pi * X / P + np.pi)) / 2)**fill_factor))
    LUT = Y + func >= 0
    # LUT[abs(X) > P] = False  # Only one structure within the unit-cell

    if has_draw is True:
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.imshow(LUT)
        plt.subplot(2, 1, 2)
        plt.plot(x, func[0, :].transpose())

    return LUT


def LUT_halfcos(NL, fill_factor, A=1., xres=10., has_draw=False):
    u, v = np.linspace(-0.5, 0.5, NL * xres), np.linspace(
        -0.5, 0.5, NL)  # increase lateral resolution is cheap
    U, V = np.meshgrid(u, v)
    D = np.ones_like(U)
    P = 1. / fill_factor

    LUT = V + A * np.cos(U * P * np.pi) - (0.5 - (v[-1] - v[-2]) / 2.) <= 0
    LUT[abs(U) > 1. / P] = 1
    D[LUT] = 0

    if has_draw is True:
        plt.figure()
        plt.imshow(LUT)

    return LUT


def LUT_triangle(NP, halfbase, xres=10, has_draw=False, **kwargs):
    x = np.linspace(-0.5, 0.5, NP * xres)
    y = np.linspace(-0.5, 0.5, NP)
    X, Y = np.meshgrid(x, y)
    deltay = (y[1] - y[0]) * 0.5
    deltax = (x[1] - x[0]) * 0.5
    m = (y.max() - y.min()) / (
        (x.max() - x.min()) / 2. - x.max() - halfbase + 4 * deltax)
    LUT = (m * X - Y + x.min() - deltay <= 0) * (-m * X - Y + x.min() - deltay
                                                 <= 0)

    if has_draw is True:
        plt.figure()
        plt.imshow(LUT)

    return np.array(LUT, dtype='bool')


def LUT_circle(NP, radius, z0=0, xres=10, has_draw=False, **kwargs):
    x = np.linspace(-0.5, 0.5, NP * xres)
    y = np.linspace(-radius[0], radius[0], NP)
    X, Y = np.meshgrid(x, y)
    LUT = (X)**2 / radius[0]**2 + (Y - z0)**2 / radius[1]**2 <= 1

    if has_draw is True:
        plt.figure()
        plt.imshow(LUT)

    return np.array(LUT, dtype='bool')
