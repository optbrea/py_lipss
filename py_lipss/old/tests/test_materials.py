# !/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Tests for Jones_vector module"""

import os

import numpy as np
from py_lipss.old.utilis.materials import load_emt_0, load_material

mater0 = "Vacuum"
mater1 = "Si"
eV = np.linspace(1., 3.5, 2)
folder = os.getcwd() + Dpath
eps0 = load_material(mater0, eV, folder=folder)
eps1 = load_material(mater1, eV, folder=folder)
epsm = load_emt_0(0.5, mater0, mater1, eV, folder=folder)
epsm_p, epsm_pt = load_emt_0(0.5, mater0, mater1, eV, anisotropy=True,
                             folder=folder)
print(eps1, eps0, epsm, epsm_p)
