#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import hickle as hkl
# import multiprocessing
from functools import partial
import argparse
# import sys
from py_lipss.pyS4 import nanoxy as qr
from py_lipss.experiments import explore1d as spectrum
# from py_lipss.utils import materials
parser = argparse.ArgumentParser()

parser.add_argument("-NC", type=int, default=4)
parser.add_argument("-NP", type=int, default=48)
parser.add_argument("-NPx", type=int, default=50)
parser.add_argument("-NPy", type=int, default=50)
parser.add_argument("-NPz", type=int, default=1)
# parser.add_argument("doe_file")
parser.add_argument("-a", type=float, default=1.293589327932066)
parser.add_argument("-G", type=int, default=1)
parser.add_argument("-H", type=float, default=0.5)
parser.add_argument("-S", type=complex, default=1.0)
parser.add_argument("-P", type=complex, default=0.0)
# parser.add_argument("savename")

args = parser.parse_args()
opts = vars(args)
print(opts)


def main():
    # doe_structure = hkl.load(opts['doe_file'])
    doe_structure = hkl.load('QR.hkl')
    opts['doe_structure'] = doe_structure['F']
    # import matplotlib.pyplot as plt
    # plt.imshow(doe_structure['F'])
    # plt.colorbar()
    # plt.show()
    # print doe_structure.keys()
    NP = args.NP
    NC = args.NC
    l = np.linspace(1.3, 0.35, NP)
    opts['layers'] = [{
        "Name": "l0",
        "Thickness": 0.,
        "Material": "Vacuum"
    }, {
        "Name": "l1",
        "Thickness": args.H,
        "Material": "Vacuum"
    }, {
        "Name": "l2",
        "Thickness": 0.,
        "Material": "Vacuum"
    }]

    # if NC > 1:
    # pool = multiprocessing.Pool(processes=NC)
    # results = pool.map(partial(run_doe,**opts), l)
    # pool.close()
    # else:
    # results = map(partial(run_doe,**opts), l)
    # # print results
    a = args.a
    print(a)
    G = args.G
    S = args.S
    P = args.P

    doe = qr.Matrix(a,
                    G=G,
                    b=a,
                    sim_options={
                        "PolarizationDecomposition": False,
                        "Verbosity": 9
                    },
                    autostart=False)
    n_si = 4.0
    k_si = 0.001
    eps_si = n_si**2 - k_si**2 + 2 * n_si * k_si * 1j
    materials = {"Vacuum": 1., "Si": eps_si}
    doe.set_materials(materials=materials)
    materials_lut = {'Vacuum': 0, 'Si': 1}
    doe_patt = doe_structure['F']
    # print doe_structure
    # print doe_patt
    doe.set_layers(layers=opts['layers'])
    dN = a / (2. * doe_patt.shape[0]) * 0.99
    #Create triangular matrix
    center_displace = a / (2. * doe_patt.shape[0])
    CTD = np.zeros(([2] + list(doe_patt.shape)))
    # print CTD.shape
    CTD[0, 1::2] = center_displace
    CTD[0, 1::2] = -center_displace
    doe.add_pattern(doe_patt,
                    materials_lut,
                    1,
                    background="Vacuum",
                    dN=dN,
                    center_d=CTD,
                    pattern_type="Circle")
    doe.set_excite(l[NP / 2], sAmplitude=S, pAmplitude=P)
    doe.get_fluxes()  # as autostart=False, this only set the object for the
    # experiment
    # print doe.setters
    # doe.start(run_calcs=True)
    print(type(doe.results))
    experiment = spectrum.SpectrumDispersion(doe, 1. / l)
    nanos = experiment.run_spectrum(NC)
    print("It, L(micron), Materials, Fluxes.shape")
    results = []
    for nano in nanos:
        print(nano.i, 1. / nano.freq, nano.materials, nano.fluxes.shape)
        results.append(nano.get_results())

    # To obtain the Postscript we can use the initial doe
    doe.start()
    # glist = np.array(doe.S.GetBasisSet())
    doe.S.OutputLayerPatternPostscript(
        "l1", "{}.ps".format("output_test_doe_matrix"))
    print(1. / doe.freq, doe.materials, doe.freq)  #doe.pattern

    # f = open('{}-results.hkl'.format(opts['savename']), 'w')
    f = open('{}-results.hkl'.format("output_test_doe_matrix"), 'w')
    hkl.dump(results, f, compression='gzip')
    f.close()


# def run_doe(l, **kwargs):
# doe = nano.Matrix(kwargs['a'], kwargs['G'], b=kwargs['a'],
# sim_options={"PolarizationDecomposition" : False, "Verbosity":0})
# n_si = 4.0
# k_si = 0.001
# eps_si = n_si**2 - k_si**2 + 2* n_si*k_si*1j
# materials = {"Vacuum": 1., "Si": eps_si}
# doe.set_materials(materials=materials)
# materials_lut = {0:'Vacuum', 1:'Si'}
# doe_patt=kwargs['doe_structure']
# # print doe_patt
# doe.set_layers(layers=opts['layers'])
# dN= kwargs['a']/(2. * doe_patt.shape[0]) * 0.99
# #Create triangular matrix
# center_displace= kwargs['a']/(2. * doe_patt.shape[0])
# CTD = np.zeros(([2] + list(doe_patt.shape)))
# # print CTD.shape
# CTD[0,1::2] = center_displace
# CTD[0,1::2] = -center_displace
# doe.add_pattern(doe_patt,materials_lut, 1, background="Vacuum",
# dN=dN,center_d=CTD,
# pattern_type="Circle")
# doe.set_excite(l, sAmplitude=kwargs['S'],
# pAmplitude=kwargs['P'])
# glist = np.array(doe.S.GetBasisSet())
# fluxes = doe.get_fluxes()
# #z = np.linspace(-0.5,1., kwargs['NPz'])
# #fields = doe.get_fields_ongrid(z=z,
# #                               NPx=kwargs['NPx'],
# #                               NPy=kwargs['NPy'],
# #                               )
# # x = np.linspace(-kwargs['a']/2., kwargs['a']/2.)
# # y = np.linspace(-kwargs['a']/2., kwargs['a']/2.)
# # epsilon = doe.get_epsilon(x=x,y=y,z=np.array([0]))
# #doe.save_results(kwargs['savename'])
# # doe.results['l'] = np.array([l,0])
# doe.S.OutputLayerPatternPostscript("l1","test1.ps")
# return dict(l=np.array([l]),fluxes=fluxes)

if __name__ == "__main__":
    main()
# nano.doe_binary_polygon(a
