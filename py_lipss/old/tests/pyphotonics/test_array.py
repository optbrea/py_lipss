#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from py_lipss.pyS4 import nanoxz
a = 1.  #[\mu m] lattice parameter in microns
G = 37  # Truncation in k-space
H = 1.  #[\mu m] Thickness of the grating
l = 0.5  #[\mu m] incident wavelength
# grating = nanoxz.NanoArray1D(a, b=a, G=G,
grating = nanoxz.NanoArray1D(a,
                             G=G,
                             sim_options={
                                 "PolarizationDecomposition": True,
                                 "Verbosity": 9
                             })
materials = {"Vacuum": 1.0, "SiO2": 1.5**2}
grating.set_materials(materials=materials)
layers = [
    {
        "Name": "l0",
        "Thickness": 0.,
        "Material": "Vacuum"
    },
    {
        "Name": "l1",
        "Thickness": H,
        "Material": "Vacuum"
    },
    {
        "Name": "l2",
        "Thickness": 0.,
        "Material": "Vacuum"
    },
]
grating.set_layers(layers=layers)  #,patterns=patterns)
# grating.S.SetRegionRectangle(**patterns[0]["Rectangle"])
x = np.array([0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0])
mat_lut = {"SiO2": 1, "Vacuum": 0}
# grating.add_pattern(x, 1, mat_lut, pattern_type="Circle")
grating.add_pattern(x, 1, mat_lut, pattern_type="Rectangle")
grating.S.OutputLayerPatternPostscript("l1", "p.ps")
grating.set_excite(l)
grating.save_siminfo("output_test_pat")
glist = grating.S.GetBasisSet()
grating.get_fluxes()
print(glist, grating.get_fluxes, grating.patterns)
