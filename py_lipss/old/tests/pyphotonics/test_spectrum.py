#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
from py_lipss.pyS4 import simS4
from py_lipss.experiment_class import explore1d
import sys
try:
    NC = int(sys.argv[1])
except:
    NC = 1
a = 0.5  #[\mu m] lattice parameter in microns
G = 31  # Truncation in k-space
H = 2.  #[\mu m] Thickness of the grating
w = 0.1  #[\mu m] width of the prism
l = 1.0  #[\mu m] incident wavelength
grating = simS4.Patterned(a,
                          G=G,
                          sim_options={
                              "PolarizationDecomposition": True,
                              "Verbosity": 0
                          },
                          autostart=False)
materials = {"Vacuum": 1.0, "Si": 3.5**2}
grating.set_materials(materials=materials)
layers = [
    {
        "Name": "l0",
        "Thickness": 0.,
        "Material": "Vacuum"
    },
    {
        "Name": "l1",
        "Thickness": H,
        "Material": "Vacuum"
    },
    {
        "Name": "l2",
        "Thickness": 0.,
        "Material": "Vacuum"
    },
]
patterns = [{
    "Rectangle": {
        "Layer": "l1",
        "Material": "Si",
        "Center": (0.0, 0.0),
        "Angle": 0.,
        "Halfwidths": (w, w)
    }
}]
grating.set_layers(layers=layers)  #,patterns=patterns)
# grating.S.SetRegionRectangle(**patterns[0]["Rectangle"])
grating.set_patterns(patterns)
grating.set_excite(l)
grating.save_siminfo("out_test_pattern")
# glist = grating.S.GetBasisSet()
grating.get_fluxes()
freq = np.linspace(0.5, 1., 100)
experiment = explore1d.SpectrumDispersion(grating, freq)
gratings = experiment.run_spectrum(NC)

for grt in gratings:
    print(grt.i, grt.freq, grt.fluxes)
