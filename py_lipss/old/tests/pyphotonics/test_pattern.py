#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from py_lipss.pyS4 import simS4
from pprint import pprint
a = 1. #[\mu m] lattice parameter in microns
G = 29 # Truncation in k-space
H = 2 #[\mu m] Thickness of the grating
w = 0.1 #[\mu m] width of the prism
l = 0.5#[\mu m] incident wavelength
grating = simS4.Patterned(a,G=G,
          sim_options={"PolarizationDecomposition":True,
                  "Verbosity":0})
materials = {"Vacuum":1.0, "SiO2":1.5**2}
grating.set_materials(materials=materials)
layers = [{"Name":"l0", "Thickness":0., "Material":"Vacuum"},
          {"Name":"l1", "Thickness":H,  "Material":"Vacuum"},
          {"Name":"l2", "Thickness":0., "Material":"Vacuum"},]
patterns = [{"Rectangle":
            {"Layer":"l1","Material":"SiO2",
             "Center":(a/2.,0.0), "Angle":0.,
             "Halfwidths":(a/4.0,0.0)}
            }
           ]
grating.set_layers(layers=layers)#,patterns=patterns)
# grating.S.SetRegionRectangle(**patterns[0]["Rectangle"])
grating.set_patterns(patterns)
grating.set_excite(l)
glist = grating.S.GetBasisSet()
pprint(grating.get_info())
grating.save_siminfo("test_pat")
glist = grating.S.GetBasisSet()
grating.get_fluxes()
pprint((glist, grating.fluxes.shape))

