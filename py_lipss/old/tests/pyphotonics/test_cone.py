#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# import numpy as np
from py_lipss.pyS4 import nanoxz
from py_lipss.utils import numpyhacks as nph
a = 0.6  #[\mu m] lattice parameter in microns
R = 0.2
NL = 10
G = 37  # Truncation in k-space
H = 1.  #[\mu m] Thickness of the grating
l = 0.5  #[\mu m] incident wavelength
grating = nanoxz.NanoArray2D(a,
                             b=a,
                             G=G,
                             sim_options={
                                 "PolarizationDecomposition": False,
                                 "Verbosity": 9
                             },
                             autostart=False)
materials = {"Vacuum": 1.0, "Si": 3.5**2}
grating.set_materials(materials=materials)
layers = [{"Name":"l0", "Thickness":0., "Material":"Vacuum"}] + \
         [{"Name":"l{}".format(i),
           "Thickness":H/float(NL),
           "Material":"Vacuum"} for i in range(1,NL+2)]  +\
         [{"Name":"l{}".format(NL+2), "Thickness":0., "Material":"Si"}]

grating.set_layers(layers=layers)
Z = nph.LUT_triangle(NL, R / a)
# import matplotlib.pyplot as plt
# plt.imshow(Z, extent=[-a/2., a/2., 0, 1])
# plt.colorbar()
# plt.show()
mat_lut = {"Si": 1, "Vacuum": 0}
grating.add_pattern(Z, range(1, 11), mat_lut, pattern_type="Circle")
print(grating.patterns)
grating.set_excite(l)
grating.save_siminfo("output_test_cone")
grating.start()
glist = grating.S.GetBasisSet()
grating.get_fluxes()
print(glist, grating.fluxes, grating.patterns)
