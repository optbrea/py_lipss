#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Before fexperiments.py

import numpy as np
from py_pol.stokes import Stokes


def get_stokes_from_E(E, **kwargs):
    """Function to calculate the Stokes parameter using the Electric field.

    Parameters:
        E (np.array): Electric Field (Ex, Ey, 0)

    Returns:
        Stokes (np.array): Stokes vector (I, Q, U, V)
    """
    print(E.shape)
    Ex, Ey = E[0, :], E[1, :]
    stk1 = Stokes('stk1')
    stk1.from_distribution(Ex=Ex, Ey=Ey)
    stks = stk1.get_list().squeeze()
    return stks
    """
    Exp = Ex / np.sqrt(np.conjugate(Ex) * Ex + np.conjugate(Ey) * Ey)
    Eyp = Ey / np.sqrt(np.conjugate(Ex) * Ex + np.conjugate(Ey) * Ey)
    Ex = Exp
    Ey = Eyp
    SS = [0, 0, 0, 0]
    SS[0] = np.conjugate(Ex) * Ex + np.conjugate(Ey) * Ey
    SS[1] = np.conjugate(Ex) * Ex - np.conjugate(Ey) * Ey
    SS[2] = 2 * (Ex * np.conjugate(Ey)).real
    SS[3] = 2 * (Ex * np.conjugate(Ey)).imag
    SS = np.array(SS)
    """


def stokes2ellipticity(I, Q, U, V, numeric_noise=1e-10):
    """Function to convert stokes parameters to ellipticity parameters
    Parameters:
    I (float) :
    Q (float) :
    U (float) :
    V (float) :

    Returns:
    A (float): semiaxis
    B (float): semiaxis
    Theta (float, 0 2 pi): Rotation angle
    h (float, -1,+1):   Rotation direction

    """

    # L = Q + 1j*U
    L_mod = np.sqrt(Q**2 + U**2)
    A = np.sqrt(1. / .2 * (I + L_mod))
    B = np.sqrt(1. / .2 * (I - L_mod))
    norm = max(A.max(), B.max())
    A = A / norm
    B = B / norm
    Theta = 0.5 * np.arctan2(U.real, Q.real)
    V[abs(V) < numeric_noise] = numeric_noise  # numerical clean
    h = np.sign(V)
    return A, B, Theta, h


def get_jones_from_grt(grt,
                       pols=(1. / np.sqrt(2.), 1. / np.sqrt(2.)),
                       location='r'):
    """Function to calculate the Jones vector at normal incidence. It can be used for reflection or tranmision
    Parameters:
        grt (pyS4.simS4): structure with the nanostructure
        pols (tuple): Input Polarization (S, P)
        location (str): 'r' for reflected or 't' for transmited ligth

    Returns:
        J0 (np.array): Jones vector for the input field
        J1 (np.array): Jones vector for the reflected field
    """

    if location == 'r':
        z_obs = np.linspace(-200 / grt.freq, -198. / grt.freq, 180)
    elif location == 't':
        z_obs = np.linspace(198 / grt.freq, 200. / grt.freq, 180)

    grt.clear_results()
    grt.source['pAmplitude'] = pols[0]
    grt.source['sAmplitude'] = pols[1]
    grt.get_fields(0, 0, z=z_obs)
    grt00 = grt.empty()

    grt.start(run_calcs=True)
    grt00.start(run_calcs=True)

    if location == 'r':
        E = grt.fields[:, 0, 0, 0] - grt00.fields[:, 0, 0, 0]
    elif location == 't':
        E = grt.fields[:, 0, 0, 0]

    J0 = get_jones_2d(grt00.fields[:, 0, 0, 0].sum(axis=0))
    J1 = get_jones_2d(E.sum(axis=0))
    return J0, J1


def get_stokes_from_grt(grt,
                        pols=(1. / np.sqrt(2.), 1. / np.sqrt(2.)),
                        location='r'):
    """Function to calculate the Jones vector at normal incidence. It can be used for reflection or tranmision
    Parameters:
        grt (pyS4.simS4): structure with the nanostructure
        pols (tuple): Input Polarization (S, P)
        location (str): 'r' for reflected or 't' for transmited ligth

    Returns:
        J0 (np.array): Jones vector for the input field
        J1 (np.array): Jones vector for the reflected field
    """

    if location == 'r':
        z_obs = np.linspace(-200 / grt.freq, -198. / grt.freq, 180)
    elif location == 't':
        z_obs = np.linspace(198 / grt.freq, 200. / grt.freq, 180)

    grt.clear_results()
    grt.source['pAmplitude'] = pols[0]
    grt.source['sAmplitude'] = pols[1]
    grt.get_fields(0, 0, z=z_obs)
    grt00 = grt.empty()

    grt.start(run_calcs=True)
    grt00.start(run_calcs=True)

    if location == 'r':
        E = grt.fields[:, 0, 0, 0] - grt00.fields[:, 0, 0, 0]
    elif location == 't':
        E = grt.fields[:, 0, 0, 0]

    E0 = grt00.fields[:, 0, 0, 0].sum(axis=0)
    E = E.sum(axis=0)

    return get_stokes_from_E(E0), get_stokes_from_E(E)


def polarization_reflection_experiment(grt,
                                       pols=(1. / np.sqrt(2.),
                                             1. / np.sqrt(2.)),
                                       z=-100.,
                                       kind='reflected'):
    """Function to calculate the stokes parameter from the experiment at normal incidence, the reflectance of the nanostructure and the 'efficiency' of the reflectance compared to a falt structure.

    Parameters:
        grt (pyS4.simS4): structure with the nanostructure
        pols (tuple): Input Polarization (S, P)
        NP (int, optional): Resolution of the field to obtain Stokes
        z (float, np.array, optional):

    Returns:
        stokes (np.array): Stokes parameters (IQUV)
        R      (np.float): Reflectance
        eta    (np.float): Reflectance_nano/Reflectance_flat"""

    grt.stop()
    grt.clear_results()
    grt.source['pAmplitude'] = pols[0]
    grt.source['sAmplitude'] = pols[1]
    grt.get_fields(0, 0, z / grt.freq)
    # z=np.linspace(-200 / grt.freq, -198. / grt.freq, NP))
    grt.get_fluxes([{"Layer": "l0", "zOffset": 0.}])
    grt00 = grt.empty()
    grt0 = grt.empty_pattern()
    grt.start(run_calcs=True)
    grt0.start(run_calcs=True)
    grt00.start(run_calcs=True)

    if kind == 'reflected':
        E = grt.fields[:, 0, 0, 0, :2] - grt00.fields[:, 0, 0, 0, :2]
    elif kind == 'transmitted':
        E = grt.fields[:, 0, 0, 0, :2]

    print(E.shape)

    if isinstance(z, float):
        stok = get_stokes_from_E(E[0, :])
    else:
        stok = [get_stokes_from_E(E[_, :]) for _ in range(E.shape[0])]
        stok = np.array(stok)

    R = grt.fluxes.sum(axis=1)[0, 1].real
    eta = R / grt0.fluxes.sum(axis=1)[0, 1].real
    return stok, -R, eta


def get_jones_polmatrix(grt):
    """Function to calculate the jones vector and the  polarization matrix from the experiment at normal incidence. Using pols (1,0) and (0,1).

    Parameters:
        grt (pyS4.simS4): structure with the nanostructure

    Returns:
        J0_mat (np.array): Polarization Matrix (2x2)
        J1_mat (np.array):Polarization Matrix (2x2) of the output
        P_mat  (np.array): Polarization Matrix (2x2)
    """

    J0s = np.zeros((2, 2), dtype='complex128')
    J1s = np.zeros((2, 2), dtype='complex128')

    # assert (len(pols) == 2)
    pols = ((1., 0.), (0., 1.))

    for _, pol in enumerate(pols):
        grt.stop()
        grt.clear_results()
        grt.source['pAmplitude'] = pol[0]
        grt.source['sAmplitude'] = pol[1]

        grt.get_fields(0, 0, -100)
        grt.get_fluxes([{"Layer": "l0", "zOffset": 0.}])
        grt00 = grt.empty()

        grt.start(run_calcs=True)
        grt00.start(run_calcs=True)

        # Puntual E_field
        E0 = grt00.fields[0, 0, 0, 0, :]
        E = grt.fields[0, 0, 0, 0, :]
        # To obtain the reflected field
        E = E - E0

        # # Field over m $\lambda$
        # E0_I = grt00.fields[:, 0, 0, 0, :].sum(axis=0)
        # E_I = grt.fields[:, 0, 0, 0, :].sum(axis=0)

        # print np.rad2deg(np.angle(E0)), np.rad2deg(np.angle(E0_I))
        # print np.rad2deg(np.angle(E)), np.rad2deg(np.angle(E_I))

        # # To obtain the reflected field
        # E_I = E_I - E0_I

        # # Remove the constant phase
        # E = np.absolute(E) * np.exp((np.angle(E) - np.angle(E_I)) *1j)
        # E0 = np.absolute(E0) * np.exp((np.angle(E0) - np.angle(E0_I)) *1j)

        # # We divide with E0 to remove the phase displacement of the incident
        # # wave.
        E = E * np.exp(-np.angle(E0) * 1j)
        # # Normalize to the incident field
        norm_rule = np.absolute(E0).max()
        E = E / norm_rule
        E0 = E0 / norm_rule

        J0 = get_jones_2d(E0)
        J1 = get_jones_2d(E)
        J0s[_, :] = J0
        J1s[_, :] = J1

    # Assuming pols = (1,0), (0,1), it is faster than solving ax=b
    # print J0s, J1s
    P_mat = np.zeros(4, dtype='complex128')
    P_mat[0] = J1s[0, 0]
    P_mat[2] = J1s[0, 1]

    P_mat[1] = J1s[1, 0]
    P_mat[3] = J1s[1, 1]
    P_mat = P_mat * np.exp(-np.angle(P_mat[0]) * 1j)

    return J0s, J1s, P_mat


def get_jones_2d(E):
    J0, J1, _ = E
    # J0_mod = np.absolute(J0)
    # J0_ang = np.angle(J0)

    # J1_mod = np.absolute(J1)
    # J1_ang = np.angle(J1)
    # J0 = J0_mod + 0j
    # J1 = J1_mod * np.exp((J1_ang  - J0_ang ) * 1j)

    return np.array([J0, J1])
