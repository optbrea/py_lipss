#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np
import scipy.interpolate


def find_nearest(array, value):
    """ rutine to find the nearest value in a numpy array"""
    idx = (np.abs(array - value)).argmin()
    return array[idx], idx


def LUT_cos(NL, P, A=1., xres=10, offset=0):
    """ Function to generate a cos/sin 2d image, z = cos(x). Under the cosine values are set
    to True, and upper to False, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis. This is useful
    for RCWA .
    Args:
        NL (int): Number of layers (discretization in height, z)
        P (float, 0-1): Normalized period of the cos function
        A (float, 0-1): Normalized Amplitude of the cos function
        xres (int >=1) : Discretization in x
        zoffset(float): Offset in z direction of the cos function.
    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (NL, xres*NL)
    """
    pi = np.pi
    x, y = np.linspace(-0.5, 0.5, NL * xres), np.linspace(-1., 1., NL)
    X, Y = np.meshgrid(x, y)
    LUT = Y - A * np.cos(2. * X * pi / P - pi) - (1. - A) + offset * 2. >= 0
    LUT[abs(X) > P / 2.] = False  #Only one structure within the unit-cell
    return LUT


def LUT_xzfun(x0, z0, NL, A=1., xres=10, offset=0, kind='linear'):
    """ Function to generate a 2d image, z = f(x). Between the maximum
    and minium value the function values are set
    to True, and upper to False. The minimum value in z will be the floor of the
    function, xres increase the resolution in the x-direction
    to increase the accuracy in the discretization for x-axis. This is useful
    for RCWA.
    Args:
        x0 (np.array): x-direction points
        z0 (np.array): z-direction points
        NL (int): Number of layers (discretization in height, z)
        A (float, 0-1): Normalized Amplitude of the function
        xres (int >=1) : Discretization in x
        zoffset(float): Offset in z direction of the function.
    Returns:
        LUT (np.array, dtype=Bool): Look up table with shape (NL, xres*NL)
    """
    x, y = np.linspace(x0.min(), x0.max(), NL * xres), np.linspace(0, 1., NL)
    X, Y = np.meshgrid(x, y)
    z0 = -z0
    z0 = (z0 - z0.min())
    z0 = z0 / z0.max()
    F = scipy.interpolate.interp1d(x0, z0, kind=kind, bounds_error=None)
    LUT = Y - F(X) + offset >= 0
    return LUT


def LUT_halfcos(NL, ff, A=1., xres=10.):
    u, v = np.linspace(-0.5, 0.5, NL * xres), np.linspace(
        -0.5, 0.5, NL)  # increase lateral resolution is cheap
    U, V = np.meshgrid(u, v)
    D = np.ones_like(U)
    P = 1. / ff
    LUT = V + A * np.cos(U * P * np.pi) - (0.5 - (v[-1] - v[-2]) / 2.) <= 0
    LUT[abs(U) > 1. / P] = 1
    D[LUT] = 0
    return LUT


def LUT_triangle(NP, halfbase, xres=10, **kwargs):
    x = np.linspace(-0.5, 0.5, NP * xres)
    y = np.linspace(-0.5, 0.5, NP)
    X, Y = np.meshgrid(x, y)
    deltay = (y[1] - y[0]) * 0.5
    deltax = (x[1] - x[0]) * 0.5
    m = (y.max() - y.min()) / (
        (x.max() - x.min()) / 2. - x.max() - halfbase + 4 * deltax)
    Z = (m * X - Y + x.min() - deltay <= 0) * (-m * X - Y + x.min() - deltay <=
                                               0)
    return np.array(Z, dtype='bool')


def LUT_circle(NP, R_a, xres=10, **kwargs):
    x = np.linspace(-0.5, 0.5, NP * xres)
    y = np.linspace(-R_a, R_a, NP)
    X, Y = np.meshgrid(x, y)
    Z = X**2 + Y**2 <= R_a**2
    return np.array(Z, dtype='bool')


def jumps(a, v, *args, **kwargs):
    """ Function to return the jumps in a True-False array:
    a: int value of the mask
    v: the array """
    lut = a == v
    dZ = lut[1:] ^ lut[:-1]
    j, = np.where(dZ)
    return j
