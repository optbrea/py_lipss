#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import pathos.multiprocessing as multiprocessing


def mapit(funct, param_list, processes):
    if processes == 1:
        return [funct for p in param_list]
    else:
        pool = multiprocessing.Pool(processes=processes)
        data = pool.map(funct, param_list)
        pool.close()
        pool.join()
        return data
