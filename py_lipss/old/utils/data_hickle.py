#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np


def extracter(DATA,
              myvars=["freq"],
              issorted=True,
              returnclass=False,
              **kwargs):
    """Extract from a list of  dictionaries, DATA, keys in myvars array, sort them with the
    first value in myvars  if issorted=False
    an convert them to a numpy array """
    if not issorted:
        DATA.sort(key=lambda x: x[myvars[0]])

    data = dict.fromkeys(myvars)
    for k in data:
        data[k] = np.array([di[k] for di in DATA])
    if returnclass:
        data = Struct(**data)
    return data


def extracter_sim(DATA, myvars=["freq"], returnclass=False, **kwargs):
    """Extract from a list of  simulation, DATA, keys in myvars array,
    an convert them to a dictionary with numpy arrays """

    data = dict.fromkeys(myvars)
    for k in data:
        data[k] = np.array([getattr(di, k) for di in DATA])
    if returnclass:
        data = Struct(**data)
    return data


class Struct:
    """Create a class from a dictionary newclass=Struct(**mydict) """

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __repr__(self):
        return '<%s>' % str('\n '.join('%s : %s' % (k, repr(v))
                                       for (k, v) in self.__dict.iteritems()))


class objdict(dict):
    """ Class to create a dictionary that supports interaction as a class d['a']
    == d.a """

    def __getattr__(self, name):
        if name in self:
            return self[name]
        else:
            raise AttributeError("No such attribute: " + name)

    def __setattr__(self, name, value):
        self[name] = value

    def __delattr__(self, name):
        if name in self:
            del self[name]
        else:
            raise AttributeError("No such attribute: " + name)
