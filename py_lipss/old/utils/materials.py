#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from numpy import conjugate, interp, loadtxt, ones_like
from scipy import constants

q = constants.e
h = constants.h
c = constants.c
eV2nm = h * c / q * 1e9
eV2m = h * c / q
pi = constants.pi
e = np.e
k = constants.k


def refraction_index_from_file(filename,
                               sheet_name,
                               column_names=('wavelength (nm)', 'n', 'k'),
                               wavelength=.85,
                               raw=False,
                               has_draw=True):
    """Gets the refraction index of steel for a given wavelength.

    Args:
        wavelength(float): wavelength of the light beam.
        raw (bool): If True it returns all the data from the xls.
    """

    data = pd.read_excel(filename, sheet_name=sheet_name)

    wavelengths = data[column_names[0]].values.astype(float)
    n = data[column_names[1]].values.astype(float)
    kappa = data[column_names[2]].values.astype(float)

    if has_draw is True:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('wavelengths (nm)')
        ax1.plot(wavelengths, n, 'r', label='n')
        ax1.set_ylabel('n', color='r')
        ax1.tick_params(axis='y', labelcolor='r')
        ax2 = ax1.twinx()
        ax2.plot(wavelengths, kappa, 'b', label=r'$\kappa$')
        ax2.set_ylabel(r'$\kappa$', color='b')
        ax2.tick_params(axis='y', labelcolor='b')
        fig.tight_layout()
        fig.legend(loc=10)

    if raw is True:
        return wavelengths, n, kappa

    else:
        z_n = np.polyfit(wavelengths, n, 6)
        z_kappa = np.polyfit(wavelengths, kappa, 6)

        f_n = np.poly1d(z_n)
        f_kappa = np.poly1d(z_kappa)

        return f_n(wavelength * 1000), f_kappa(wavelength * 1000)


def refraction_index_steel(wavelength, raw=False, has_draw=True):
    """Gets the refraction index of steel for a given wavelength.

    Args:
        wavelength(float): wavelength of the light beam.
        raw (bool): If True it returns all the data from the xls.
    """

    script_dir = os.path.dirname(__file__)
    rel_path = "../data/refraction_index_steel.xls"
    abs_file_path = os.path.join(script_dir, rel_path)
    data = pd.read_excel(abs_file_path, sheet_name='n_steel')

    wavelengths = data['wavelength (nm)'].values.astype(float)
    n = data['n'].values.astype(float)
    kappa = data['kappa'].values.astype(float)

    if has_draw is True:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('wavelengths (nm)')
        ax1.plot(wavelengths, n, 'r', label='n')
        ax1.set_ylabel('n', color='r')
        ax1.tick_params(axis='y', labelcolor='r')
        ax2 = ax1.twinx()
        ax2.plot(wavelengths, kappa, 'b', label=r'$\kappa$')
        ax2.set_ylabel(r'$\kappa$', color='b')
        ax2.tick_params(axis='y', labelcolor='b')
        fig.tight_layout()
        fig.legend(loc=10)

    if raw is True:
        return wavelengths, n, kappa

    else:
        z_n = np.polyfit(wavelengths, n, 6)
        z_kappa = np.polyfit(wavelengths, kappa, 6)

        f_n = np.poly1d(z_n)
        f_kappa = np.poly1d(z_kappa)

        return f_n(wavelength * 1000), f_kappa(wavelength * 1000)


def load_material(material,
                  eV,
                  folder,
                  force_k=-1.0,
                  nk=False,
                  verbose=False):
    """ Load the permittivity of a material from file.

    This function will atomatically locate the file containing the
    permittivity from the material name. The material file should
    be named as: <material name>_<rest_of_description>.dat. For
    example, for material name GaAs the file should be "GaAs_nk_Palik.dat"
    The data must be organized by columns: eV n k.

    Args:
        material: Material name as a string.
        eV: Energy discretization in electron-Volts.
        folder: Folder to look for the file. Default value
            os.path.dirname(__file__) + "/DATA/index/".
        force_k: If >= 0.0, forces the imaginary refractive index (k)
            take that value at all wavelengths.
        verbose: If True, print the input files matching the material name.
        nk: If True return the complex refractive index instead of the
            permittivity.

    Returns:
        A numpy array with the complex permittivity.
    """

    ld = os.listdir(folder)  # list_index_files
    lm = [l for l in ld if material == l.split('_')[0]]

    if len(lm) > 1:
        wrn_txt = "Warning. More than one file found for material {:s}.\n".format(
            material)
        wrn_txt += "         Only the first occurence will be loaded.\n"
        wrn_txt += "         List of files:\n"
        sys.strerr.write(wrn_txt)
        sys.strerr.write("\n".join(lm))

    if verbose:
        print(lm)

    eV0, n, k = loadtxt(folder + lm[0], unpack=True)

    n = interp(eV, eV0, n)

    if force_k >= 0.0:
        k = force_k * ones_like(eV)
    else:
        k = interp(eV, eV0, k)

    if nk:
        return n + 1.0j * k
    else:
        return n**2 - k**2 + 2 * n * k * 1j


def load_material_nk(material, eV, folder, force_k=-1.0, verbose=False):
    """Loads the complex refractive index from file.

    This function actually call load_material to read the
    data file with nk=True. See above for further details.
    """
    return load_material(material=material,
                         eV=eV,
                         folder=folder,
                         force_k=force_k,
                         verbose=verbose,
                         nk=True)


def load_effective_medium(ff,
                          material0,
                          material1,
                          eV,
                          folder,
                          force_k=[-1., -1],
                          verbose=False,
                          anisotropy=False):
    """ Recreate  the simplest effective medium theory recreating a composite
    material as the weighted sum of both materials (matrix=material0, and
    inclusions=material1) using ff: [float] the filling factor. """
    eps0 = load_material(material0,
                         eV,
                         folder=folder,
                         force_k=force_k[0],
                         verbose=verbose)
    eps1 = load_material(material1,
                         eV,
                         folder=folder,
                         force_k=force_k[0],
                         verbose=verbose)

    emt_pd = ff * eps1 + (1. - ff) * eps0
    if anisotropy:
        emt_pp = eps0 * eps1 / ((1 - ff) * eps1 + ff * eps0)
        return emt_pd, emt_pp
    else:
        return emt_pd
