#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from skimage import measure
import numpy as np


# import matplotlib.pyplot as plt
def polygonize(A, a=1.):
    """ Create polygons from a square matrix with 1 and 0,
    the axis is centered with size [-a/2,a/2,..]
    A: square matrix
    returns dictionary with a direct polygon, and a simplified one.
    """
    gimg = np.zeros((A.shape[0] + 2, A.shape[1] + 2))
    gimg[1:-1, 1:-1] = A  # To create closed polygons
    contours = measure.find_contours(gimg, 0.5)
    app_contours = []
    c_vals = []
    # fig, (ax1, ax2) = plt.subplots(ncols=2, figsize=(9, 4))
    # ax1.imshow(A, origin='lower', cmap='binary',
    # alpha=0.5,
    # extent=[-0.5, 0.5, -0.5, 0.5],
    # interpolation='nearest')

    # ax2.imshow(A, origin='lower', cmap='binary',
    # alpha=0.5,
    # extent=[-0.5, 0.5, -0.5, 0.5],
    # interpolation='nearest')

    for n, contour in enumerate(contours):
        c_val = gimg[int(contour[0, 0]) + 1, int(contour[0, 1])]
        c_vals.append(abs(1 - c_val))
        if c_val == 1:
            color = 'red'
        else:
            color = 'black'

        # Cleaning edges without polygon #
        contour[contour > A.shape[0]] = A.shape[0] + 1
        contour[contour < 1] = 0

        # Create polygons (needs positive values)
        new_s = contour.copy()
        appr_s = measure.approximate_polygon(new_s, tolerance=0.5)
        # Move to system -0.5 to 0.5 a = 1
        a = A.shape[0]

        contour = contour - a / 2.
        contour = contour / a

        appr_s = appr_s - a / 2.
        appr_s = appr_s / a
        #print appr_s > 0.5
        appr_s[appr_s > 0.5] = 0.5
        appr_s[appr_s < -0.5] = -0.5

        app_contours.append(appr_s[::-1])
    A_P = {
        "A_P0": {
            "CONTOURS": contours,
            "MATERIALS": c_vals
        },
        "A_Ps": {
            "CONTOURS": app_contours,
            "MATERIALS": c_vals
        }
    }
    return A_P
