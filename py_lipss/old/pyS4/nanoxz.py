#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import numpy as np

from . import simS4


class NanoArray1D(simS4.Patterned):
    """ Class to add pattern methods based on an 1D array to simS4 object.

    The pattern will be applied to the selected layer. It is encoded as
    a 1D dimensional integer array where each element represents a
    section of the unit cell as |a|/n_elements and a material. For
    example, [0, 0, 1, 1] represents a 1D pattern where 2/4 of the unit
    cell are made of material 0 and the other 2/4 of material 1. It
    would hence represent a 1D grating of 50 % filling factor.
    """

    # def _jumps(self, a, v, *args, **kwargs):
    # lut = a == v
    # dZ = lut[1:] ^ lut[:-1]
    # j, = np.where(dZ)
    # return j
    def _data2pattern(self,
                      array_pattern,
                      maters,
                      internal_res=1e4,
                      background="Vacuum",
                      *args,
                      **kwargs):
        """Auxiliary function to transform the 1D pattern array in a structure.

        From the integer array and materials look up table, a list of
        regions is generated. A region is defined by a material, a center and
        a halfwidth which can be plugged into the S4 structure definition.

        Args:
            array_pattern: 1D integer array. Each elements represents a
                section of the unit cell (|a|/len(array_pattern)) made of
                material maters[array_pattern[n]].
            maters: Dictionary containing the materials involved in the
                definition of the unit cell as {"material name": integer}.
                It works as a look up table.
            internal_res: Step along the array to identify domains.
            background: background material name.
        """
        from scipy import interpolate
        array_pattern0 = np.zeros((array_pattern.size + 2))
        array_pattern0[1:-1] = array_pattern
        array_pattern = array_pattern0
        array_pattern[0], array_pattern[-1] = [maters[background]] * 2
        x0 = np.linspace(0, 1., array_pattern.size)
        x = np.linspace(0, 1., internal_res)
        fx = interpolate.interp1d(x0, array_pattern, kind='nearest')
        a = fx(x)
        halfwidths, centers, materials = [], [], []
        for k, v in maters.items():
            if k != background:
                j = nph.jumps(a, v)
                # print "j", j
                if j.shape[0] > 0:

                    if j.shape[0] % 2 == 1:
                        j = np.pad(j, (0, 1),
                                   mode='constant',
                                   constant_values=a.shape[0] - 1)
                    # print k,j
                    xi = x[j] + (x[1] - x[0]
                                 ) / 2.  # Find jumps and scale with half pixel
                    hws = (xi[1::2]
                           - xi[::2]) / 2.  # Find halfwidths using jumps
                    # Quick and dirty patch to cope with lattice vectors instead of lattice
                    # constants.
                    xi = xi * self._a[0] - self._a[0] / \
                        2.  # Scale with lattice
                    hws = hws * self._a[0]
                    halfwidths.append(hws)
                    centers.append(xi[::2] + hws)  # Center is first jump + hw
                    materials.append(k)

        return materials, halfwidths, centers

    def add_pattern(self,
                    x,
                    layer,
                    mater_lut,
                    pattern_type="Rectangle",
                    internal_res=1000,
                    ccx=0.0,
                    ccy=0.,
                    hwy=0.,
                    **kwargs):
        """Generates a pattern based on the integer pattern enconded in x.

        Args:
            x: Rank 1 integer np.ndarray. Each element denotes a given material
                to be searched in mater_lut.
            layer: Layers in which apply the pattern.
            mater_lut: Dictionary with materials and integer indices, e.g. {"Si":1, "Vacuum":0}
            patter_type: Sting. Only "Rectangle" and "Circle" are implemented.
            internal_res: Integer. It states the minimum x step to guess the halfwidth of
                the pattern along the x axis.
            ccx: float. A shift in the x direction of the pattern encoded in xy.
            ccy: float. A shift in the y direction of the pattern encoded in xy.
            hwy: float. Halfwidth in the y direction, i.e. a change of aspect ratio.
                If hwy < 0, aspect ratio 1 is assumed (hwx=hwy)
        """
        # layers = [l["Name"] for l in self.layers]
        # print type(layers),layers
        # layeri = layers.index[layer]
        layeri = layer
        background = self.layers[layeri]["Material"]
        materials, halfwidths, centers = self._data2pattern(x, mater_lut,
                                                            internal_res=internal_res, background=background)
        patterns = []
        for i, m in enumerate(materials):
            for cc, hw in zip(centers[i], halfwidths[i]):
                if hwy < 0:
                    hw_y = hw
                else:
                    hw_y = hwy  # avoid losing value in loop
                patts = {
                    pattern_type: {
                        "Layer": self.layers[layer]["Name"],
                        "Material": m,
                        "Center": (cc + ccx, ccy),
                    }
                }
                if pattern_type == "Rectangle":
                    patts[pattern_type]["Angle"] = 0.
                    patts[pattern_type]["Halfwidths"] = (hw, hw_y)
                elif pattern_type == "Circle":
                    r = hw
                    patts[pattern_type]["Radius"] = r
                patterns.append(patts)
        if len(patterns) > 0:
            self.setters["patterns"] = None
            if self.patterns is None:
                # self.patterns = []
                self.patterns = patterns
                self.sim_info.add("patterns")
            else:
                self.patterns.extend(patterns)

        else:
            pass
        if self.autostart:
            self._set_patterns()


class NanoArray2D(NanoArray1D):
    """ Class to add pattern methods based on an 2D array to simS4 object.

    Overload of add_pattern method of NanoArray1D to cope with 2D arrays.
    The array describes the XZ plane. This allows to easily create axially symmetric
    structures like cylinders or cones.
    """

    def add_pattern(self,
                    xy,
                    layers,
                    mater_lut,
                    pattern_type="Rectangle",
                    internal_res=1000,
                    ccx=0.,
                    ccy=0.,
                    hwy=-1.0,
                    **kwargs):
        """ The function calls add_pattern from NanoArray1D.

        Args:
            xy: Rank 2 integer np.ndarray. Each element denotes a given material
                to be searched in mater_lut.
            layers: The number of the layers where to apply the pattern. Note that
                len(layers) == xy.shape[0] must fulfill.
            mater_lut: Dictionary with materials and integer indices, e.g. {"Si":1, "Vacuum":0}
            patter_type: Sting. Only "Rectangle" and "Circle" are implemented.
            internal_res: Integer. It states the minimum x step to guess the halfwidth of
                the pattern along the x axis.
            ccx: float. A shift in the x direction of the pattern encoded in xy.
            ccy: float. A shift in the y direction of the pattern encoded in xy.
            hwy: float. Halfwidth in the y direction, i.e. a change of aspect ratio.
                If hwy < 0, aspect ratio 1 is assumed (hwx=hwy)
        """

        for i, layer in enumerate(layers):
            NanoArray1D.add_pattern(self,
                                    xy[i, :],
                                    layer,
                                    mater_lut,
                                    pattern_type=pattern_type,
                                    internal_res=internal_res,
                                    ccx=ccx,
                                    ccy=ccy,
                                    hwy=hwy,
                                    **kwargs)
