#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt
import numpy as np
from matplotlib import gridspec

from .field_parameters import get_EH_2D, get_EH_3D


def double_figure(sim, matrix1, matrix2, title1='', title2='', suptitle0=''):
    """Draws a double figure. It can be used, for example, to draw real and imaginary part of structure, etc.

    Author:
        Luis Miguel Sanchez Brea: 190215

    Parameters:
        sim (simS4.Patterned): Definition
        matrix1 (numpy.array)
        matrix2 (numpy.array).
        title1 (str)
        title2 (str)
        suptitle (str)

    Returns:
        fig:
        ax: (ax0, ax1)
        im: (im0, im1)
        cbar: cbar
    """

    a = sim._a[0]
    z = sim.fields_monitors[2]
    extension = [z.min(), z.max(), -a / 2, a / 2]

    fig = plt.figure(figsize=(12, 4))
    gs = gridspec.GridSpec(2, 1, height_ratios=[1, 1])

    ax0 = plt.subplot(gs[0])
    im0 = ax0.imshow(matrix1.T, extent=extension, aspect='auto')
    ax0.set_title(title1)
    cbar = ax0.figure.colorbar(im0, ax=ax0)

    ax1 = plt.subplot(gs[1])
    im1 = ax1.imshow(matrix2.T, extent=extension, aspect='auto')
    ax1.set_title(title2)
    cbar = ax1.figure.colorbar(im1, ax=ax1)
    im0.set_cmap('gray')
    im1.set_cmap('gray')

    fig.suptitle(suptitle0)
    plt.tight_layout()

    return fig, (ax0, ax1), (im0, im1), cbar


def plot_Jones_ellipses_in_out(exp, exp0, exp0R, kind='reflected'):

    E_input = exp0.fields[:, 0, 0, 0]
    if kind == 'reflected':
        E_reflected = exp.fields[:, 0, 0, 0] - exp0.fields[:, 0, 0, 0]
        E = E_reflected
    elif kind == 'transmitted':
        E_transmitted = exp.fields[:, 0, 0, 0]
        E = E_transmitted

    fg = plt.figure()
    ax1 = fg.add_subplot(1, 2, 1, aspect='equal')
    ax1.title.set_text('Input')
    ax1.plot(E_input[:, 0].real, E_input[:, 1].real)
    ax2 = fg.add_subplot(1, 2, 2, aspect='equal')
    ax2.plot(E[:, 0].real, E[:, 1].real)
    ax1.set_xlim(-1, 1)
    ax1.set_ylim(-1, 1)
    ax2.set_xlim(-1, 1)
    ax2.set_ylim(-1, 1)

    R = -exp.fluxes[0, :, 1].sum().real
    R0 = -exp0R.fluxes[0, :, 1].sum().real
    ax2.title.set_text("Output R={:2.2f}, Eff. {:2.2f} %".format(
        R, 100 * R / R0))
    fg.tight_layout()
    return fg


def plot2D_eps(sim, z=None, orientation='horizontal', **kwargs):
    """
    orientation (str): ='horizontal', 'vertical', 'abs'
    """

    eps = sim.eps[:, :, 0]

    if z is None:
        z = sim.fields_monitors[2]

    a = sim._a[0]

    fg1 = plt.figure(figsize=(8, 4))
    if orientation == 'horizontal':
        ax1 = fg1.add_subplot(121)
        im1 = ax1.imshow(eps.T.real,
                         extent=[z.min(), z.max(), -a / 2, a / 2],
                         aspect='auto')

        ax2 = fg1.add_subplot(122)
        im2 = ax2.imshow(eps.T.imag,
                         extent=[z.min(), z.max(), -a / 2, a / 2],
                         aspect='auto')

        ax1.set_ylabel(r'x ($\mu$m)')
        ax2.set_ylabel(r'x ($\mu$m)')
        ax1.set_xlabel(r'z ($\mu$m)')
        ax2.set_xlabel(r'z ($\mu$m)')

        im1.set_cmap('gray')
        im2.set_cmap('gray')

        plt.colorbar(im1, ax=ax1)
        plt.colorbar(im2, ax=ax2)

    elif orientation == 'vertical':
        ax1 = fg1.add_subplot(211)
        ax2 = fg1.add_subplot(212)
        im1 = ax1.imshow(eps.T.real.transpose(),
                         extent=[-a / 2, a / 2,
                                 z.min(), z.max()],
                         aspect='auto')
        im2 = ax2.imshow(eps.T.imag.transpose(),
                         extent=[-a / 2, a / 2,
                                 z.min(), z.max()],
                         aspect='auto')

        ax1.set_ylabel(r'z ($\mu$m)')
        ax2.set_ylabel(r'z ($\mu$m)')
        ax1.set_xlabel(r'x ($\mu$m)')
        ax2.set_xlabel(r'x ($\mu$m)')

        im1.set_cmap('gray')
        im2.set_cmap('gray')

        plt.colorbar(im1, ax=ax1)
        plt.colorbar(im2, ax=ax2)

    elif orientation == 'abs':
        ax1 = fg1.add_subplot(111)
        im1 = ax1.imshow(np.abs(eps.T.transpose()),
                         extent=[-a / 2, a / 2,
                                 z.min(), z.max()],
                         aspect='auto')

        ax1.set_ylabel(r'z ($\mu$m)')
        ax1.set_xlabel(r'x ($\mu$m)')

        im1.set_cmap('gray')

        plt.colorbar(im1, ax=ax1)

    fg1.tight_layout()


def plot2D_E2H2(sim,
                sim00=None,
                z=None,
                fields_og=False,
                is_normalized=True,
                **kwargs):

    E, H = get_EH_2D(sim)

    if z is None:
        z = sim.fields_monitors[2]
    a = sim._a[0]

    if sim00 is None:
        E00 = 0
        H00 = 0
    else:
        E00, H00 = get_EH_2D(sim00)

        eps = sim.eps[:, :, 0]
        is_vacuum = (np.abs(eps) == 1)
        vacuum = np.zeros_like(eps, dtype=bool)
        vacuum[is_vacuum] = True

        E00 = E00 * vacuum
        H00 = H00 * vacuum

    E2 = (E - E00)**2
    H2 = (H - H00)**2

    if is_normalized is True:
        E2 = E2 / E2.max()
        H2 = H2 / H2.max()

    fg1 = plt.figure(figsize=(8, 5))
    ax1 = fg1.add_subplot(211)
    ax2 = fg1.add_subplot(212)
    im1 = ax1.imshow(np.log(E2.T.real + 1),
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')
    im2 = ax2.imshow(np.log(H2.T.real + 1),
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')

    plt.colorbar(im1, ax=ax1)
    plt.colorbar(im2, ax=ax2)

    ax2.set_xlabel(r'z ($\mu$m)')
    ax1.set_ylabel(r'x ($\mu$m)')
    ax2.set_ylabel(r'x ($\mu$m)')

    ax1.set_title(r'$E^2$', loc='left')
    ax2.set_title(r'$H^2$', loc='left')

    im1.set_cmap('gist_heat')  #RdBu, hot
    im2.set_cmap('gist_heat')

    if is_normalized is True:
        im1.set_clim(0, 1)
        im2.set_clim(0, 1)

    fg1.tight_layout()
    return fg1


def plot2D_EH_3D(sim, z=None, fields_og=False, is_normalized=True, **kwargs):

    E, H = get_EH_3D(sim)

    if z is None:
        z = sim.fields_monitors[2]
    a = sim._a[0]

    E2 = E**2
    H2 = H**2

    if is_normalized is True:
        E2 = E2 / E2.max()
        H2 = H2 / H2.max()

    fg1 = plt.figure(figsize=(12, 4))
    ax1 = fg1.add_subplot(231)
    ax2 = fg1.add_subplot(232)
    ax3 = fg1.add_subplot(233)
    ax4 = fg1.add_subplot(234)
    ax5 = fg1.add_subplot(235)
    ax6 = fg1.add_subplot(236)

    im1 = ax1.imshow(E2[:, :, 0].T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')
    im2 = ax2.imshow(E2[:, :, 1].T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')
    im3 = ax3.imshow(E2[:, :, 2].T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')
    im4 = ax4.imshow(H2[:, :, 0].T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')
    im5 = ax5.imshow(H2[:, :, 1].T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')
    im6 = ax6.imshow(H2[:, :, 2].T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')

    plt.colorbar(im3, ax=ax3)
    plt.colorbar(im6, ax=ax6)
    #
    # ax2.set_xlabel(r'z ($\mu$m)')
    # ax1.set_ylabel(r'x ($\mu$m)')
    # ax2.set_ylabel(r'x ($\mu$m)')
    #
    ax1.set_title(r'$E^2$', loc='left')
    ax4.set_title(r'$H^2$', loc='left')
    #
    im1.set_cmap('gist_heat')  #RdBu, hot
    im2.set_cmap('gist_heat')
    im3.set_cmap('gist_heat')
    im4.set_cmap('gist_heat')
    im5.set_cmap('gist_heat')
    im6.set_cmap('gist_heat')

    # if is_normalized is True:
    #     im1.set_clim(0, 1)
    #     im2.set_clim(0, 1)

    fg1.tight_layout()
    return fg1


def plot2D_orders(data):
    glist = data.glist
    fluxes_out = data.fluxes
    amps_out = data.amps
    G = glist.shape[0]

    fg2 = plt.figure()
    ax1 = fg2.add_subplot(211)
    ax2 = fg2.add_subplot(212)

    ax1.plot(glist[:, 0], fluxes_out[0, :, 1] / fluxes_out[0, 0, 1], 'ko')
    ax2.plot(glist[:, 0], -amps_out[0, 1, :G].real, 'o', label='Real')
    ax2.plot(glist[:, 0], -amps_out[0, 1, :G].imag, 'o', label='Imag.')
    ax2.set_xlabel("D. order")
    ax1.set_ylabel("Diff. Eff.")
    ax2.set_ylabel("Amplitude")
    ax2.legend(loc=0, frameon=False)
    return fg2
