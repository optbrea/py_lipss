#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# import py_lipss.experiment_class
# import matplotlib.pyplot as plt

# before it was jones4polarizations.py

import argparse
import copy
import multiprocessing
import time

import hickle as hkl
import matplotlib.pyplot as plt
import numpy as np

from py_pol.jones_vector import Jones_vector
from py_pol.stokes import Stokes

from .computation import compute_3_structures
from .experiment_class import Experiment
from .field_parameters import get_E_0D, get_E_1D, get_efficiency_reflectance
from .polarization_parameters import (get_jones_2d, get_jones_from_grt,
                                      get_stokes_from_E, get_stokes_from_grt,
                                      polarization_reflection_experiment)
from .pyS4 import nanoxz, simS4
from .shapes import LUT_cos, LUT_cos2
from .structures import binary_grating
from .utils import numpyhacks, plotters
from .utils_drawing import plot2D_E2H2, plot2D_eps, plot_Jones_ellipses_in_out

um = 1.


def stokes_from_amplitudes_binary(args_field, args_grating, G, has_draw=False):

    period = args_grating['period']  # a
    height = args_grating['height']  # h
    n_complex = args_grating['n_complex']  # refraction index of the grating
    fill_factor = args_grating['fill_factor']

    wavelength = args_field['wavelength']
    # incident_field = args_field['incident_field']

    n = np.real(n_complex)
    k = np.imag(n_complex)

    epssteel = n**2 - k**2 + 2 * n * k * 1j
    materials = {"Vacuum": 1.0, "Steel": epssteel}
    materials_names = ["Vacuum", "Steel"]

    layers, patterns = binary_grating(materials_names, period, height,
                                      fill_factor)
    # layers = [
    #     {
    #         "Name": "l0",
    #         "Thickness": 0.,
    #         "Material": "Vacuum"
    #     },
    #     {
    #         "Name": "l1",
    #         "Thickness": height,
    #         "Material": "Vacuum"
    #     },
    #     {
    #         "Name": "l2",
    #         "Thickness": 0.,
    #         "Material": "Steel"
    #     },
    # ]
    #
    # patterns = [{
    #     "Rectangle": {
    #         "Layer": "l1",
    #         "Material": "Steel",
    #         "Center": (0, 0),
    #         "Angle": 0.,
    #         "Halfwidths": (period * fill_factor / 2., 0)
    #     }
    # }]

    grating = simS4.Patterned(period, G=G)

    grating.set_materials(materials=materials)

    grating.set_layers(layers=layers)

    grating.set_patterns(patterns=patterns)

    grating.set_excite(wavelength, pAmplitude=1, sAmplitude=0)
    amps_out1 = grating.get_amps()
    Ex = -amps_out1[1, 0, G]

    np.abs(Ex), np.angle(Ex) * (180 / np.pi)

    grating.set_excite(wavelength, pAmplitude=0, sAmplitude=1)
    amps_out2 = grating.get_amps()
    Ey = -amps_out2[0, 1, 0]

    np.abs(Ey), np.angle(Ey) * (180 / np.pi)

    j0 = Jones_vector()
    j0.from_elements(Ex, Ey)

    if has_draw is True:
        j0.draw_ellipse()

    s1 = Stokes(name='s_theo')
    s1.from_Jones(j0)

    return s1


def get_stokes_from_binary_grating(args_field,
                                   args_grating,
                                   G,
                                   num_data=(100, 100),
                                   has_draw=True):
    """
    We generate a binary grating. Propagate a certain distance and get
    the polarization parameters.

    Parameters:
        args_field (dictionary): parameters of the incident field
            wavelength (float): wavelength of the incoming light.
            incident_field (2x1 incident field): polarization of the incident field.

        args_grating (dictionary): parameters of the grating
            period - period of the grating (a)
            height - thickness of the grating
            fill_factor - fill factor of the grating
            n_complex: refraction index of the material (n+i.kappa)

            G (int): Truncation of k-space

    nsteel = 2.79897354
    ksteel = 3.96088216

    Returns:
        Stokes: `s3 = number * s2`
    """

    num_x, num_z = num_data

    period = args_grating['period']  # a
    height = args_grating['height']  # h
    n_complex = args_grating['n_complex']  # refraction index of the grating
    fill_factor = args_grating['fill_factor']

    wavelength = args_field['wavelength']
    incident_field = args_field['incident_field']

    n = np.real(n_complex)
    k = np.imag(n_complex)

    epssteel = n**2 - k**2 + 2 * n * k * 1j
    materials = {"Vacuum": 1.0, "Steel": epssteel}

    z_near = np.linspace(-.5, height + .5, num_z)
    z_far = np.linspace(-200 * wavelength, -198. * wavelength, num_z)
    x = np.linspace(-period / 2, period / 2., num_x)

    options = {"PolarizationDecomposition": False, "Verbosity": 0}

    grating = simS4.Patterned(period,
                              G=G,
                              sim_options=options,
                              autostart=False)

    epssteel = n**2 - k**2 + 2 * n * k * 1j
    materials = {"Vacuum": 1.0, "Material": epssteel}
    grating.set_materials(materials=materials)
    materials_names = ["Vacuum", "Material"]

    layers, patterns = binary_grating(materials_names, period, height,
                                      fill_factor)
    # layers = [
    #     {
    #         "Name": "l0",
    #         "Thickness": 0.,
    #         "Material": "Vacuum"
    #     },
    #     {
    #         "Name": "l1",
    #         "Thickness": height,
    #         "Material": "Vacuum"
    #     },
    #     {
    #         "Name": "l2",
    #         "Thickness": 0.,
    #         "Material": "Material"
    #     },
    # ]
    # patterns = [{
    #     "Rectangle": {
    #         "Layer": "l1",
    #         "Material": "Material",
    #         "Center": (0, 0),
    #         "Angle": 0.,
    #         "Halfwidths": (period * fill_factor / 2, 0)
    #     }
    # }]

    grating.set_layers(layers=layers)
    grating.set_patterns(patterns=patterns)
    grating.set_excite(wavelength)

    grating.get_fluxes()
    grating.get_amps()
    grating.get_fields(x=x, y=[0], z=z_near)
    grating.get_epsilon(x=x, y=[0], z=z_near)
    grating.a = grating._a
    grating.z = z_near

    grating2, no_grating, no_cell = compute_3_structures(grating)

    if has_draw is True:
        plot2D_eps(grating)
        plot2D_E2H2(grating)
        plot2D_E2H2(no_grating)
        plot2D_E2H2(no_cell)

    # Setting the nanostructure to test
    # grt = copy.deepcopy(grating) - cambio por grt=grating
    grt = grating
    grt.stop(clear_results=True)
    grt.source['pAmplitude'] = incident_field[0]
    grt.source['sAmplitude'] = incident_field[1]
    grt.get_fields(0, 0, z=z_far)
    grt.get_fluxes()
    grt.start(run_calcs=True)

    grt2, no_grating, no_cell = compute_3_structures(grt)

    if has_draw is True:
        plot_Jones_ellipses_in_out(grt2, no_cell, no_grating)

    E = grt2.fields[:, 0, 0, 0] - no_cell.fields[:, 0, 0, 0]
    E0 = no_cell.fields[:, 0, 0, 0]

    s0 = Stokes('s0')
    s0.from_E(E0, is_normalized=False)

    s1 = Stokes('s1')
    s1.from_E(E, is_normalized=False)

    return s0, s1, grating


def get_stokes_from_grating(args_field,
                            func,
                            args_grating,
                            G=21,
                            num_data=(100, 100),
                            has_draw=True):
    """
    We generate a sinusoidal grating. Propagate a certain distance and get
    the polarization parameters.

    Parameters:
        args_field (dictionary): parameters of the incident field
            wavelength (float): wavelength of the incoming light.
            incident_field (2x1 incident field): polarization of the incident field.

        func (function): function with definition of grating shape

        args_grating (dictionary): parameters of the grating
            period - period of the grating (a)
            height - thickness of the grating
            ?? fill_factor - fill factor of the grating
            n_complex: refraction index of the material (n+i.kappa)

            G (int): Truncation of k-space
            has_draw (bool): if True draws figures and ellipses

    nsteel = 2.79897354
    ksteel = 3.96088216

    Returns:
        Stokes: `s3 = number * s2`
    """

    num_x, num_z = num_data

    period = args_grating['period']  # a
    height = args_grating['height']  # h
    n_complex = args_grating['n_complex']  # refraction index of the grating
    fill_factor = args_grating['fill_factor']

    wavelength = args_field['wavelength']
    incident_field = args_field['incident_field']

    n = np.real(n_complex)
    k = np.imag(n_complex)

    epssteel = n**2 - k**2 + 2 * n * k * 1j
    materials = {"Vacuum": 1.0, "Steel": epssteel}

    NP = 100  # resolution of the fields
    NL = 13  # Even number for perfect alignment

    Hi = height / NL

    options = {"PolarizationDecomposition": False, "Verbosity": 0}

    layers = [{"Name": "l0", "Thickness": 0, "Material": "Vacuum"}]
    for iii in range(1, NL + 1):
        layers.append({
            "Name": "l{}".format(iii),
            "Thickness": Hi,
            "Material": "Vacuum"
        })

    layers.append({
        "Name": "l{}".format(iii + 1),
        "Thickness": 0.,
        "Material": "Steel"
    })

    D = func(NL, 1.)
    mater_lut = {"Vacuum": 0, "Steel": 1}
    layers_pat = range(1, NL + 1)

    if has_draw is True:
        plt.figure()
        plt.imshow(D)
        print(D.shape)

    patterns = [{
        "Rectangle": {
            "Layer": "l1",
            "Material": "Steel",
            "Center": (0, 0),
            "Angle": 0.,
            "Halfwidths": (period * fill_factor, 0)
        }
    }]

    # Define the structure using py_lipss
    grating = nanoxz.NanoArray2D(period, G=G, options=options)
    grating.set_materials(materials=materials)
    grating.set_layers(layers)

    grating.add_pattern(D, range(1, NL + 1), mater_lut)
    grating.set_excite(wavelength)
    # orders_list = grating.orders_list

    # talbot_length = period**2 / wavelength
    # z = np.linspace(-0.1*(talbot_length), 0.1*(talbot_length)+ H, NP)
    z = np.linspace(-.5, height + .5, NP)
    x = np.linspace(-period / 2, period / 2., NP)

    grating.get_fluxes()
    grating.get_amps()

    grating.get_fields(x=x, y=[0], z=z)
    grating.get_epsilon(x=x, y=[0], z=z)
    grating.a = grating._a
    grating.z = z

    if has_draw is True:
        fg = plotters.plot2D_E2H2Eps(grating)

    grating.sim_info.add('patterns')

    # Setting the nanostructure to test
    grating.sim_info
    grt = copy.deepcopy(grating)
    grt.stop(clear_results=True)
    grt.source['pAmplitude'] = incident_field[0]
    grt.source['sAmplitude'] = incident_field[1]
    grt.get_fields(0,
                   0,
                   z=np.linspace(-200 * wavelength, -198. * wavelength, NP))
    grt.get_fluxes()
    grt.start(run_calcs=True)

    grt2, no_grating, no_cell = compute_3_structures(grt)

    if has_draw is True:
        plot_Jones_ellipses_in_out(grt2, no_cell, no_grating)

    E = grt2.fields[:, 0, 0, 0] - no_cell.fields[:, 0, 0, 0]
    E0 = no_cell.fields[:, 0, 0, 0]

    s0 = Stokes('s0')
    s0.from_E(E0, is_normalized=False)

    s1 = Stokes('s1')
    s1.from_E(E, is_normalized=False)

    return s0, s1


def get_stokes_from_cos_grating(args_field,
                                args_grating,
                                G=21,
                                num_data=(100, 100),
                                has_draw=True):
    """
    We generate a sinusoidal grating. Propagate a certain distance and get
    the polarization parameters.

    Parameters:
        args_field (dictionary): parameters of the incident field
            wavelength (float): wavelength of the incoming light.
            incident_field (2x1 incident field): polarization of the incident field.

        args_grating (dictionary): parameters of the grating
            period - period of the grating (a)
            height - thickness of the grating
            ?? fill_factor - fill factor of the grating
            n_complex: refraction index of the material (n+i.kappa)

            G (int): Truncation of k-space
            has_draw (bool): if True draws figures and ellipses

    nsteel = 2.79897354
    ksteel = 3.96088216

    Returns:
        Stokes: `s3 = number * s2`
    """

    num_x, num_z = num_data

    period = args_grating['period']  # a
    height = args_grating['height']  # h
    n_complex = args_grating['n_complex']  # refraction index of the grating
    fill_factor = args_grating['fill_factor']

    wavelength = args_field['wavelength']
    incident_field = args_field['incident_field']

    n = np.real(n_complex)
    k = np.imag(n_complex)

    epssteel = n**2 - k**2 + 2 * n * k * 1j
    materials = {"Vacuum": 1.0, "Steel": epssteel}
    materials = {"Vacuum": 1.0, "Steel": epssteel}

    NP = 100  # resolution of the fields
    NL = 13  # Even number for perfect alignment

    Hi = height / NL

    options = {"PolarizationDecomposition": False, "Verbosity": 0}

    layers = [{"Name": "l0", "Thickness": 0, "Material": "Vacuum"}]
    for iii in range(1, NL + 1):
        layers.append({
            "Name": "l{}".format(iii),
            "Thickness": Hi,
            "Material": "Vacuum"
        })

    layers.append({
        "Name": "l{}".format(iii + 1),
        "Thickness": 0.,
        "Material": "Steel"
    })

    D = LUT_cos(NL, 1.)
    mater_lut = {"Vacuum": 0, "Steel": 1}
    layers_pat = range(1, NL + 1)

    if has_draw is True:
        plt.figure()
        plt.imshow(D)
        print(D.shape)

    patterns = [{
        "Rectangle": {
            "Layer": "l1",
            "Material": "Steel",
            "Center": (0, 0),
            "Angle": 0.,
            "Halfwidths": (period * fill_factor, 0)
        }
    }]

    # Define the structure using py_lipss
    grating = nanoxz.NanoArray2D(period, G=G, options=options)
    grating.set_materials(materials=materials)
    grating.set_layers(layers)

    grating.add_pattern(D, range(1, NL + 1), mater_lut)
    grating.set_excite(wavelength)
    # orders_list = grating.orders_list

    # talbot_length = period**2 / wavelength
    # z = np.linspace(-0.1*(talbot_length), 0.1*(talbot_length)+ H, NP)
    z = np.linspace(-2., height + 2., NP)
    x = np.linspace(-period / 2, period / 2., NP)

    grating.get_fluxes()
    grating.get_amps()

    grating.get_fields(x=x, y=[0], z=z)
    grating.get_epsilon(x=x, y=[0], z=z)
    grating.a = grating._a
    grating.z = z

    if has_draw is True:
        fg = plotters.plot2D_E2H2Eps(grating)

    grating.sim_info.add('patterns')

    # Setting the nanostructure to test
    grating.sim_info
    grt = copy.deepcopy(grating)
    grt.stop(clear_results=True)
    grt.source['pAmplitude'] = incident_field[0]
    grt.source['sAmplitude'] = incident_field[1]
    grt.get_fields(0,
                   0,
                   z=np.linspace(-200 * wavelength, -198. * wavelength, NP))
    grt.get_fluxes()
    grt.start(run_calcs=True)

    grt2, no_grating, no_cell = compute_3_structures(grt)

    if has_draw is True:
        plot_Jones_ellipses_in_out(grt2, no_cell, no_grating)

    E = grt2.fields[:, 0, 0, 0] - no_cell.fields[:, 0, 0, 0]
    E0 = no_cell.fields[:, 0, 0, 0]

    s0 = Stokes('s0')
    s0.from_E(E0, is_normalized=False)

    s1 = Stokes('s1')
    s1.from_E(E, is_normalized=False)

    return s0, s1, grt


def get_stokes_from_cos2_grating(
    args_field,
    args_grating,
    G=21,
    num_data=(100, 100),
    NL=13,  # Even number for perfect alignment
    #NP=100,  # resolution of the fields
    has_draw=True):
    """
    We generate a sinusoidal grating. Propagate a certain distance and get
    the polarization parameters.

    Parameters:
        args_field (dictionary): parameters of the incident field
            wavelength (float): wavelength of the incoming light.
            incident_field (2x1 incident field): polarization of the incident field.

        args_grating (dictionary): parameters of the grating
            period - period of the grating (a)
            height - thickness of the grating
            ?? fill_factor - fill factor of the grating
            n_complex: refraction index of the material (n+i.kappa)

            G (int): Truncation of k-space
            has_draw (bool): if True draws figures and ellipses

    nsteel = 2.79897354
    ksteel = 3.96088216

    Returns:
        Stokes: `s3 = number * s2`
    """

    num_x, num_z = num_data

    period = args_grating['period']  # a
    height = args_grating['height']  # h
    n_complex = args_grating['n_complex']  # refraction index of the grating
    fill_factor = args_grating['fill_factor']

    wavelength = args_field['wavelength']
    incident_field = args_field['incident_field']

    n = np.real(n_complex)
    k = np.imag(n_complex)

    epssteel = n**2 - k**2 + 2 * n * k * 1j
    materials = {"Vacuum": 1.0, "Steel": epssteel}

    Hi = height / NL

    options = {"PolarizationDecomposition": False, "Verbosity": 0}

    layers = [{"Name": "l0", "Thickness": 0, "Material": "Vacuum"}]
    for iii in range(1, NL + 1):
        layers.append({
            "Name": "l{}".format(iii),
            "Thickness": Hi,
            "Material": "Vacuum"
        })

    layers.append({
        "Name": "l{}".format(iii + 1),
        "Thickness": 0.,
        "Material": "Steel"
    })

    D = LUT_cos2(NL, fill_factor=fill_factor)
    mater_lut = {"Vacuum": 0, "Steel": 1}
    layers_pat = range(1, NL + 1)

    if has_draw is True:
        plt.figure()
        plt.imshow(D)
        print(D.shape)

    patterns = [{
        "Rectangle": {
            "Layer": "l1",
            "Material": "Steel",
            "Center": (0, 0),
            "Angle": 0.,
            "Halfwidths": (period * fill_factor, 0)
        }
    }]

    # Define the structure using py_lipss
    grating = nanoxz.NanoArray2D(period, G=G, options=options)
    grating.set_materials(materials=materials)
    grating.set_layers(layers)

    grating.add_pattern(D, range(1, NL + 1), mater_lut)
    grating.set_excite(wavelength)
    # orders_list = grating.orders_list

    # talbot_length = period**2 / wavelength
    # z = np.linspace(-0.1*(talbot_length), 0.1*(talbot_length)+ H, NP)
    z = np.linspace(-.5, height + .5, num_z)
    x = np.linspace(-period / 2, period / 2., num_x)

    grating.get_fluxes()
    grating.get_amps()

    grating.get_fields(x=x, y=[0], z=z)
    grating.get_epsilon(x=x, y=[0], z=z)
    grating.a = grating._a
    grating.z = z

    if has_draw is True:
        fg = plotters.plot2D_E2H2Eps(grating)

    grating.sim_info.add('patterns')

    # Setting the nanostructure to test
    grating.sim_info
    grt = copy.deepcopy(grating)
    grt.stop(clear_results=True)
    grt.source['pAmplitude'] = incident_field[0]
    grt.source['sAmplitude'] = incident_field[1]
    grt.get_fields(0,
                   0,
                   z=np.linspace(-200 * wavelength, -198. * wavelength, num_z))
    grt.get_fluxes()
    grt.start(run_calcs=True)

    grt2, no_grating, no_cell = compute_3_structures(grt)

    if has_draw is True:
        plot_Jones_ellipses_in_out(grt2, no_cell, no_grating)

    E = grt2.fields[:, 0, 0, 0] - no_cell.fields[:, 0, 0, 0]
    E0 = no_cell.fields[:, 0, 0, 0]

    s0 = Stokes('s0')
    s0.from_E(E0, is_normalized=False)

    s1 = Stokes('s1')
    s1.from_E(E, is_normalized=False)

    return s0, s1, grt


def sim_binary(args):
    NC = args.NC
    NP = args.NP
    a = args.a
    G = args.G
    wavelength = args.l
    # G = 31 # Truncation in k-space
    H = 0.580  # [\mu m] Thickness of the grating
    # w = 5. # [\mu m] width of the prism
    # l = 0.88# [\mu m] incident wavelength
    grating = simS4.Patterned(a,
                              G=G,
                              sim_options={
                                  "PolarizationDecomposition": False,
                                  "Verbosity": 0
                              },
                              autostart=False)
    nsteel = 2.79897354
    ksteel = 3.96088216
    epssteel = nsteel**2 - ksteel**2 + 2 * nsteel * ksteel * 1j
    materials = {"Vacuum": 1.0, "Steel": epssteel}
    grating.set_materials(materials=materials)
    layers = [
        {
            "Name": "l0",
            "Thickness": 0.,
            "Material": "Vacuum"
        },
        {
            "Name": "l1",
            "Thickness": H,
            "Material": "Vacuum"
        },
        {
            "Name": "l2",
            "Thickness": 0.,
            "Material": "Steel"
        },
    ]
    patterns = [{
        "Rectangle": {
            "Layer": "l1",
            "Material": "Steel",
            "Center": (0, 0),
            "Angle": 0.,
            "Halfwidths": (a / 6., 0)
        }
    }]
    grating.set_layers(layers=layers)
    grating.set_patterns(patterns=patterns)
    grating.set_excite(wavelength)

    Hs = np.linspace(0.0, 1., NP)
    FF = np.linspace(0.0, 1., NP)
    xd0s = []
    t1 = time.time()
    ij = 0
    for i, h in enumerate(Hs):
        for j, fill_factor in enumerate(FF):
            xd0s.append(
                dict(grating=grating, fill_factor=fill_factor, h=h, ij=[ij]))
            ij += 1

    if NC == 1:
        data = [bin_grt_parallel(xd) for xd in xd0s]
    else:
        pool = multiprocessing.Pool(processes=NC)
        data = pool.map(bin_grt_parallel, xd0s)
        pool.close()
        pool.join()
    print("Finished", time.time() - t1, "s")
    savename = "binary_jones4polMap_a_{a}_G_{G}_l_{wavelength}".format(
        **args.__dict__)
    savefile = open("{}.hkl".format(savename), 'w')
    hkl.dump(data, savefile, compression='gzip')
    savefile.close()


def sim_binary_deprecated(args):
    NC = args.NC
    NP = args.NP
    a = args.a
    G = args.G
    wavelength = args.l
    # G = 31 # Truncation in k-space
    H = 0.580  # [\mu m] Thickness of the grating
    # w = 5. # [\mu m] width of the prism
    # l = 0.88# [\mu m] incident wavelength
    grating = simS4.Patterned(a,
                              G=G,
                              sim_options={
                                  "PolarizationDecomposition": False,
                                  "Verbosity": 0
                              },
                              autostart=False)
    nsteel = 2.79897354
    ksteel = 3.96088216
    epssteel = nsteel**2 - ksteel**2 + 2 * nsteel * ksteel * 1j
    materials = {"Vacuum": 1.0, "Steel": epssteel}
    grating.set_materials(materials=materials)
    layers = [
        {
            "Name": "l0",
            "Thickness": 0.,
            "Material": "Vacuum"
        },
        {
            "Name": "l1",
            "Thickness": H,
            "Material": "Vacuum"
        },
        {
            "Name": "l2",
            "Thickness": 0.,
            "Material": "Steel"
        },
    ]
    patterns = [{
        "Rectangle": {
            "Layer": "l1",
            "Material": "Steel",
            "Center": (0, 0),
            "Angle": 0.,
            "Halfwidths": (a / 6., 0)
        }
    }]
    grating.set_layers(layers=layers)
    grating.set_patterns(patterns=patterns)
    grating.set_excite(wavelength)

    Hs = np.linspace(0.0, 1., NP)
    FF = np.linspace(0.0, 1., NP)
    xd0s = []
    t1 = time.time()
    ij = 0
    for i, h in enumerate(Hs):
        for j, fill_factor in enumerate(FF):
            xd0s.append(
                dict(grating=grating, fill_factor=fill_factor, h=h, ij=[ij]))
            ij += 1

    if NC == 1:
        data = [bin_grt_parallel(xd) for xd in xd0s]
    else:
        pool = multiprocessing.Pool(processes=NC)
        data = pool.map(bin_grt_parallel, xd0s)
        pool.close()
        pool.join()
    print("Finished", time.time() - t1, "s")
    savename = "binary_jones4polMap_a_{a}_G_{G}_l_{wavelength}".format(
        **args.__dict__)
    savefile = open("{}.hkl".format(savename), 'w')
    hkl.dump(data, savefile, compression='gzip')
    savefile.close()


def bin_grt_parallel(xd):
    print(xd['ij'][0])
    fill_factor = xd['fill_factor']
    h = xd['h']
    grating = xd['grating']
    grt = copy.deepcopy(grating)
    grt.patterns[0]["Rectangle"]["Halfwidths"] = (fill_factor / 2. * grt._a, 0)
    grt.layers[1]["Thickness"] = h
    # grt.restart_sim()
    # fluxes = grt.get_fluxes()

    pols = ((1, 0), (0, 1), (1. / np.sqrt(2.), 1. / np.sqrt(2.)),
            (2. / np.sqrt(2.), 2. / np.sqrt(2.) * (-1j)))
    myexp = Experiment(grt, pols=pols)
    myexp.run_experiment()
    wavelength = 1. / grt.freq
    stokes = myexp.get_stokes_z(-10 * wavelength)[2]
    fluxes = [exp.get_fluxes() for exp in myexp.exps]
    jones = myexp.get_jones_2d(np.linspace(-10 * wavelength, -20 * wavelength))
    return dict(fluxes=fluxes, jones=jones, stokes=stokes, ij=xd['ij'])


def sim_sin(args):
    NC = args.NC
    NP = args.NP
    a = args.a
    G = args.G
    wavelength = args.l
    NL = args.NL  # Even number for perfect alignment
    H = 0.580  # [\mu m] Thickness of the grating
    Hi = H / NL

    nsteel = 2.79897354
    ksteel = 3.96088216
    epssteel = nsteel**2 - ksteel**2 + 2 * nsteel * ksteel * 1j
    materials = {"Vacuum": 1.0, "Steel": epssteel}
    grating = nano_arrays.NanoArray2D(a,
                                      G=G,
                                      sim_options={
                                          "PolarizationDecomposition": False,
                                          "Verbosity": 0
                                      },
                                      autostart=False)

    grating.set_materials(materials=materials)
    layers = [{"Name": "l0", "Thickness": 0, "Material": "Vacuum"}]
    for iii in range(1, NL + 1):
        layers.append({
            "Name": "l{}".format(iii),
            "Thickness": Hi,
            "Material": "Vacuum"
        })

    layers.append({
        "Name": "l{}".format(iii + 1),
        "Thickness": 0.,
        "Material": "Steel"
    })

    L = numpyhacks.LUT_cos(NL, 0.5)
    D = np.zeros_like(L)
    D[L] = 1
    mater_lut = {"Vacuum": 0, "Steel": 1}
    layers_pat = range(1, NL + 1)
    grating.set_layers(layers=layers)
    grating.add_pattern(D, layers_pat, mater_lut, pattern_type="Rectangle")
    grating.set_excite(wavelength)
    Hs = np.linspace(0.0, 1., NP)
    FF = np.linspace(0.0, 1., NP)
    xd0s = []
    t1 = time.time()
    ij = 0
    # import matplotlib.pyplot as plt
    for j, fill_factor in enumerate(FF):
        if fill_factor > 0:
            L = numpyhacks.LUT_cos(NL, fill_factor)
            D = np.zeros_like(L)
            D[L] = 1
        else:
            D = np.zeros((NL, NL * 10))
        for i, h in enumerate(Hs):
            # plt.figure('{}'.format(fill_factor))
            # plt.imshow(D)
            hi = h / NL
            layers = [{"Name": "l0", "Thickness": 0, "Material": "Vacuum"}]
            for iii in range(1, NL + 1):
                layers.append({
                    "Name": "l{}".format(iii),
                    "Thickness": hi,
                    "Material": "Vacuum"
                })
            layers.append({
                "Name": "l{}".format(iii + 1),
                "Thickness": 0.,
                "Material": "Steel"
            })
            xd0s.append(
                dict(grating=grating,
                     ij=[ij],
                     layers=layers,
                     D=D,
                     layers_pat=layers_pat,
                     mater_lut=mater_lut))
            # print(xd0s[-1]['layers'])
            ij += 1
    # plt.show()
    if NC == 1:
        data = [sin_grt_parallel(xd) for xd in xd0s]
    else:
        pool = multiprocessing.Pool(processes=NC)
        data = pool.map(sin_grt_parallel, xd0s)
        pool.close()
        pool.join()

    print("Finished", time.time() - t1, "s")
    savename = "sin_jones4polMap_a_{a}_G_{G}_l_{wavelength}".format(
        **args.__dict__)
    savefile = open("{}.hkl".format(savename), 'w')
    hkl.dump(data, savefile, compression='gzip')
    savefile.close()


def sin_grt_parallel(xd):
    print(xd['ij'][0])
    grating = xd['grating']
    grt = copy.deepcopy(grating)
    grt.layers = xd['layers']
    grt.patterns = None
    # grt.restart_sim()
    grt.add_pattern(xd['D'],
                    xd['layers_pat'],
                    xd['mater_lut'],
                    pattern_type="Rectangle")
    # grt.restart_sim()
    pols = ((1, 0), (0, 1), (1. / np.sqrt(2.), 1. / np.sqrt(2.)),
            (2. / np.sqrt(2.), 2. / np.sqrt(2.) * (-1j)))
    myexp = Experiment(grt, pols=pols)
    myexp.run_experiment()
    wavelength = 1. / grt.freq
    stokes = myexp.get_stokes_z(-10 * wavelength)[2]
    fluxes = [exp.get_fluxes() for exp in myexp.exps]
    jones = myexp.get_jones_2d(np.linspace(-10 * wavelength, -20 * wavelength))
    return dict(fluxes=fluxes, jones=jones, stokes=stokes, ij=xd['ij'])


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-NC", type=int, default=4)
    parser.add_argument("-NP", type=int, default=48)
    parser.add_argument("-NL", type=int, default=3)
    parser.add_argument("-nano", default="binary")
    parser.add_argument("-a", type=float, default=0.58)
    parser.add_argument("-G", type=int, default=1)
    parser.add_argument("-l", type=float, default=0.88)
    args = parser.parse_args()
    nano = args.nano
    functions = {
        "binary": sim_binary,
        "sin": sim_sin,
    }
    run = {k: False for k in functions.keys()}
    run[nano] = True
    for fun in functions.keys():
        if run[fun]:
            functions[fun](args)


if __name__ == '__main__':
    main()
