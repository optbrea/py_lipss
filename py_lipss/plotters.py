#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import warnings

import matplotlib.pyplot as plt
import numpy as np


def draw_LUT(LUT, x_f, z_f, axis, interpolation='bilinear', cmap=None):
    extent = [x_f[0], x_f[-1], z_f[0], z_f[-1], ]
    im = plt.imshow(np.flipud(LUT),
                    origin='lower',
                    interpolation=interpolation,
                    extent=extent,
                    cmap=cmap)
    plt.axis(axis)
    return im


def draw_field(u, x_f, z_f, axis, interpolation='bilinear', cmap=None):
    extent = [z_f[0], z_f[-1], x_f[0], x_f[-1]]
    im = plt.imshow(u.transpose(),
                    origin='lower',
                    interpolation=interpolation,
                    extent=extent,
                    cmap=cmap)
    plt.axis(axis)
    return im


def imshow_serial(matrix,
                  shape_plots,
                  axis=0,
                  post_edit=False,
                  list_params=None,
                  **kwargs):
    """ Function to plot matrix arrays as 2D plots
    Args:
        matrix (np.array): data to plot
        shape_plots (tuple): Subplot grid (2,1) == 2 row, 1 column
        axis (int) : Axis to iterate
        list_params (list of dicts):params to pass to each subplot, it
                                    overrites the kwargs in the subplot
        post_edit (boolean): return the image and axis objetcs to
    """

    figargs = kwargs.get('figargs', dict(dpi=130))
    fg = plt.figure(**figargs)
    # TODO move code to fg, axes = plt.subplot()
    xlabel = kwargs.get('xlabel', "")
    ylabel = kwargs.get('ylabel', "")
    total = shape_plots[0] * shape_plots[1]
    titles = kwargs.get('titles', [" "] * total)
    cbar_param = kwargs.get('cbar_parm', dict(fraction=0.046,
                                              pad=0.04))  # magic
    params = kwargs.get(
        'params',
        dict(extent=[0, 1, 0, 1],
             aspect='equal',
             interpolation='none',
             origin='lower'))
    cmaps = kwargs.get('cmaps', [params.get('cmap', 'RdYlBu')] * total)

    slc = [slice(None)] * len(matrix.shape)

    k = 0
    images = []
    axes = []
    if list_params is None:
        list_params = [{}] * total

    if 'last_axis' in kwargs and kwargs['last_axis']:
        axis = -1
    for i in range(shape_plots[0]):
        for j in range(shape_plots[1]):
            try:
                ax = fg.add_subplot("{}{}{}".format(shape_plots[0],
                                                    shape_plots[1], k + 1))
                ax.set_xlabel(xlabel)
                ax.set_ylabel(ylabel)
                ax.title.set_text(titles[k])
                params["cmap"] = cmaps[k]
                slc[axis] = k
                parm = dict(params)
                parm.update(list_params[k])
                im = ax.imshow(matrix[tuple(slc)], **parm)
                images.append(im)
                plt.colorbar(im, ax=ax, **cbar_param)
                plt.axis('tight')
            except:
                ax.axis('off')
                warnings.warn("shape_plots bigger than matrix", Warning)

            axes.append(ax)
            k += 1
    fg.tight_layout()
    if post_edit:
        return fg, axes, images
    else:
        return fg


def plot2D_EH(E, H, xyz=None, axis='equal'):
    """Determines the fields from the fields parameter

    xyz=z0S

    """
    Ex = E[:, :, 0].squeeze()
    Ey = E[:, :, 1].squeeze()
    Ez = E[:, :, 2].squeeze()
    Hx = H[:, :, 0].squeeze()
    Hy = H[:, :, 1].squeeze()
    Hz = H[:, :, 2].squeeze()

    S_max = np.real(
        np.max((Ex.real, Ey.real, Ez.real, Hx.real, Hy.real, Hz.real)))
    S_min = np.real(
        np.min((Ex.real, Ey.real, Ez.real, Hx.real, Hy.real, Hz.real)))

    dims = np.shape(Ex)
    num_dims = len(dims)

    if num_dims == 1:
        z0 = xyz
        if z0 is None:
            z0 = np.linspace(0, 1, dims[0])

        plt.figure()
        plt.subplot(3, 2, 1)
        plt.plot(z0, np.real(Ex))
        plt.title("$E_x$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 3)
        plt.plot(z0, np.real(Ey))
        plt.title("$E_y$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 5)
        plt.plot(z0, np.real(Ez))
        plt.title("$E_z$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 2)
        plt.plot(z0, np.real(Hx))
        plt.title("$H_x$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 4)
        plt.plot(z0, np.real(Hy))
        plt.title("$H_y$")
        plt.ylim(S_min, S_max)

        plt.subplot(3, 2, 6)
        plt.plot(z0, np.real(Hz))
        plt.title("$H_z$")
        plt.ylim(S_min, S_max)

        plt.suptitle("E, H fields")

    elif num_dims == 2:
        x0, z0 = xyz
        if z0 is None:
            z0 = np.linspace(0, 1, dims[1])
        if x0 is None:
            x0 = np.linspace(0, 1, dims[0])

        fig, axes = plt.subplots(nrows=3, ncols=2)
        plt.subplot(3, 2, 1)
        im0 = draw_field(np.real(Ex), x0, z0, axis, cmap='seismic')
        plt.title("$E_x$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 3)
        draw_field(np.real(Ey), x0, z0, axis, cmap='seismic')
        plt.title("$E_y$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 5)
        draw_field(np.real(Ez), x0, z0, axis, cmap='seismic')
        plt.title("$E_z$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 2)
        draw_field(np.real(Hx), x0, z0, axis, cmap='seismic')
        plt.title("$H_x$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 4)
        draw_field(np.real(Hy), x0, z0, axis, cmap='seismic')
        plt.title("$H_y$")
        plt.clim(S_min, S_max)

        plt.subplot(3, 2, 6)
        draw_field(np.real(Hz), x0, z0, axis, cmap='seismic')
        plt.title("$H_z$")
        plt.clim(S_min, S_max)

        cb_ax = fig.add_axes([0.1, 0, 0.8, 0.05])
        cbar = fig.colorbar(im0, cax=cb_ax, orientation='horizontal')

        return fig


def plot_E2H2(E, H, xyz=None, has_draw=True, axis='equal'):
    E2 = np.linalg.norm(E, axis=-1)**2
    H2 = np.linalg.norm(H, axis=-1)**2

    Ex = E[:, :, 0].squeeze()

    if has_draw:
        dims = np.shape(Ex)
        num_dims = len(dims)
        if num_dims == 1:
            z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[0])
            plt.figure()
            plt.subplot(1, 2, 1)
            plt.plot(z0, E2.real)
            plt.title("$E^2$")

            plt.subplot(1, 2, 2)
            plt.plot(z0, H2.real)
            plt.title("$H^2$")

        elif num_dims == 2:
            x0, z0 = xyz
            if z0 is None:
                z0 = np.linspace(0, 1, dims[1])
            if x0 is None:
                x0 = np.linspace(0, 1, dims[0])

            fig, axes = plt.subplots(nrows=2, ncols=1)
            plt.subplot(2, 1, 1)
            plt.title("$E^2$")
            im1 = draw_field(E2.real, x0, z0, axis)

            plt.subplot(2, 1, 2)
            plt.title("$H^2$")
            im2 = draw_field(H2.real, x0, z0, axis)

            cb_ax = fig.add_axes([0.1, 0, 0.8, 0.05])
            cbar = fig.colorbar(im2, cax=cb_ax, orientation='horizontal')

    return E2, H2


def plot2D_eps(eps, xyz=None, axis='scaled'):

    eps = eps.squeeze()

    dims = np.shape(eps)
    num_dims = len(dims)
    if num_dims == 1:
        z0 = xyz
        if z0 is None:
            z0 = np.linspace(0, 1, dims[0])
        plt.figure()
        plt.subplot(2, 1, 1)
        plt.plot(z0, np.abs(eps))
        plt.title("$\epsilon$")

    elif num_dims == 2:
        x0, z0 = xyz
        if z0 is None:
            z0 = np.linspace(0, 1, dims[1])
        if x0 is None:
            x0 = np.linspace(0, 1, dims[0])

        x0, z0 = xyz
        draw_field(np.abs(eps), x0, z0, axis)
        plt.colorbar(orientation='vertical')

        plt.title("$\epsilon$")
        plt.clim(vmin=1)
        plt.tight_layout()


def plot2D_E2H2Eps(sim, z=None, fields_og=False, **kwargs):
    if fields_og:
        fields = sim.fields_og
        E = fields[:, 0, :, 0, :]
        H = fields[:, 1, :, 0, :]
    else:
        fields = sim.fields
        E = fields[:, :, 0, 0, :]
        H = fields[:, :, 0, 1, :]
    eps = sim.eps[:, :, 0]
    if z is None:
        z = sim.fields_monitors[2]
    a = sim._a[0]
    E2 = np.linalg.norm(E, axis=-1)**2
    H2 = np.linalg.norm(H, axis=-1)**2
    E2 = E2 / E2.max()
    H2 = H2 / H2.max()
    fg1 = plt.figure(figsize=(8, 7))
    ax1 = fg1.add_subplot(311)
    ax2 = fg1.add_subplot(312)
    ax3 = fg1.add_subplot(313)
    im1 = ax1.imshow(E2.T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')
    im2 = ax2.imshow(H2.T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto')
    im3 = ax3.imshow(eps.T.real,
                     extent=[z.min(), z.max(), -a / 2, a / 2],
                     aspect='auto',
                     cmap='binary')
    plt.colorbar(im1, ax=ax1)
    plt.colorbar(im2, ax=ax2)
    plt.colorbar(im3, ax=ax3)
    ax3.set_xlabel(r'z ($\mu$m)')
    ax1.set_ylabel(r'x ($\mu$m)')
    ax2.set_ylabel(r'x ($\mu$m)')
    ax3.set_ylabel(r'x ($\mu$m)')

    ax1.set_title(r'$E^2$', loc='left')
    ax2.set_title(r'$H^2$', loc='left')
    ax3.set_title(r'$\epsilon_r$', loc='left')
    fg1.tight_layout()
    return fg1


def plot2D_orders(data, T=10):
    glist = data.glist
    fluxes_out = data.fluxes
    amps_out = data.amps
    G = glist.shape[0]
    fg2 = plt.figure()
    ax1 = fg2.add_subplot(211)
    ax2 = fg2.add_subplot(212)
    ax1.plot(glist[:, 0], fluxes_out[0, :, 1] / fluxes_out[0, 0, 1], 'ko')
    ax2.plot(glist[:, 0], -amps_out[0, 1, :G].real, 'o', label='Real')
    ax2.plot(glist[:, 0], -amps_out[0, 1, :G].imag, 'o', label='Imag.')
    ax1.set_xlim(-15, 15)
    ax2.set_xlim(-15, 15)
    ax2.set_xlabel("D. order")
    ax1.set_ylabel("Diff. Eff.")
    ax2.set_ylabel("Amplitude")
    ax2.legend(loc=0, frameon=False)
    return fg2


def plot_ellipse(E):

    fg = plt.figure()
    ax1 = fg.add_subplot(1, 1, 1, aspect='equal')
    ax1.plot(E[:, 0].real, E[:, 1].real, 'k')
    plt.grid('on')
    plt.axis('equal')
    ax1.set_xlim(-1, 1)
    ax1.set_ylim(-1, 1)

    return fg


#
# def plot_input_output_myexp(myexp):
#     fg = plt.figure()
#     i = 1
#     I = len(myexp.exps)
#     for exp, exp0 in zip(myexp.exps, myexp._empty_exp):
#         E = exp.E_lz
#         E0 = exp0.E_lz
#         ax1 = fg.add_subplot(I, 2, i, aspect='equal')
#         ax1.title.set_text('Input')
#         ax1.plot(E0[:, 0].real, E0[:, 1].real)
#         ax2 = fg.add_subplot(I, 2, i + 1, aspect='equal')
#         ax2.plot(E[:, 0].real, E[:, 1].real)
#         R = -exp.fluxes[0, :, 1].sum().real
#         ax2.title.set_text("Output R={0:.2f}".format(R))
#         i = i + 2
#     fg.tight_layout()
#     return fg
#
#
# def plot_input_output(exp, exp0, exp0R):
#     fg = plt.figure()
#     E = exp.fields[:, 0, 0, 0] - exp0.fields[:, 0, 0, 0]
#     E0 = exp0.fields[:, 0, 0, 0]
#
#     ax1 = fg.add_subplot(1, 2, 1, aspect='equal')
#     ax1.title.set_text('Input')
#     ax1.plot(E0[:, 0].real, E0[:, 1].real)
#     ax2 = fg.add_subplot(1, 2, 1 + 1, aspect='equal')
#     ax2.plot(E[:, 0].real, E[:, 1].real)
#
#     R = -exp.fluxes[0, :, 1].sum().real
#     R0 = -exp0R.fluxes[0, :, 1].sum().real
#     print(R0)
#     ax2.title.set_text("Output R={0:.2f}, Eff. {1:.2f}%".format(R, R / R0))
#     fg.tight_layout()
#     return fg
