#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import py_lipss
from numpy import loadtxt
from scipy import constants

q = constants.e
h = constants.h
c = constants.c
eV2nm = h * c / q * 1e9
eV2m = h * c / q
pi = constants.pi
e = np.e
k = constants.k


def index_to_permitivity(n_complex):
    """permitivity of a material.

    Arguments:
        n_complex (np.float or np.complex): refraction index of a material

    returns:
        (np.complex): permitivity of material.
    """

    n_index = n_complex.real
    k_index = n_complex.imag

    eps_material = n_index**2 - k_index**2 + 2 * n_index * k_index * 1j

    return eps_material


def load_material_excel(filename, is_nm=True, folder=None, has_draw=True):
    """Gets the refraction index of steel for a given wavelength.

    Args:
        filename (str): Filename
        sheet_name (str): If several
        column_names (str): Column names
        raw (bool): If True it returns all the data from the xls.
        wavelength(float): wavelength of the light beam used when raw = False
    """

    if folder is None:
        path = os.path.dirname(__file__)
        print(path)
        folder = path + "/data/"

    data = pd.read_excel(folder + filename)

    wavelengths = np.array(data['wavelength'])
    n = np.array(data['n'])
    kappa = np.array(data['k'])

    if is_nm:
        wavelengths = wavelengths / 1000

    if has_draw is True:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('wavelengths (um)')
        ax1.plot(wavelengths, n, 'r', label='n')
        ax1.set_ylabel('n', color='r')
        ax1.tick_params(axis='y', labelcolor='r')
        ax2 = ax1.twinx()
        ax2.plot(wavelengths, kappa, 'b', label=r'$\kappa$')
        ax2.set_ylabel(r'$\kappa$', color='b')
        ax2.tick_params(axis='y', labelcolor='b')
        fig.tight_layout()
        fig.legend()

    nc = n + 1j * kappa

    return wavelengths, nc


def load_material_dat(filename,
                      folder=None,
                      is_nm=True,
                      verbose=False,
                      has_draw=False):
    """ Load the permittivity of a material from file.

    This function will atomatically locate the file containing the
    permittivity from the material name. The material file should
    be named as: <material name>_<rest_of_description>.dat. For
    example, for material name GaAs the file should be "GaAs_nk_Palik.dat"
    The data must be organized by columns: eV n k.

    Args:
        filename: Material name as a string.
        eV: Energy discretization in electron-Volts.
        folder: Folder to look for the file. Default value
            os.path.dirname(__file__) + "/DATA/index/".
        force_k: If >= 0.0, forces the imaginary refractive index (k)
            take that value at all wavelengths.
        verbose: If True, print the input files matching the material name.
        nk: If True return the complex refractive index instead of the
            permittivity.

    Returns:
        wavelenghts (np.array): Wavelengths
        n_complex (np.array): complex refraction index
    """
    if folder is None:
        path = os.path.dirname(__file__)
        print(path)
        folder = path + "/data/"

    wavelengths, n, kappa = loadtxt(folder + filename, unpack=True)

    if is_nm:
        wavelengths = wavelengths / 1000

    nc = n + 1.0j * kappa

    if has_draw is True:
        fig, ax1 = plt.subplots()
        ax1.set_xlabel('wavelengths (um)')
        ax1.plot(wavelengths, n, 'r', label='n')
        ax1.set_ylabel('n', color='r')
        ax1.tick_params(axis='y', labelcolor='r')
        ax2 = ax1.twinx()
        ax2.plot(wavelengths, kappa, 'b', label=r'$\kappa$')
        ax2.set_ylabel(r'$\kappa$', color='b')
        ax2.tick_params(axis='y', labelcolor='b')
        fig.tight_layout()
        fig.legend()

    return wavelengths, nc


def interp_refraction_index(wavelengths_interp, wavelengths, nc):
    """"""
    n = np.real(nc)
    kappa = np.imag(nc)

    n_interp = np.interp(wavelengths_interp, wavelengths, n)
    kappa_interp = np.interp(wavelengths_interp, wavelengths, kappa)

    nc_interp = n_interp + 1j * kappa_interp

    return nc_interp


def load_effective_medium(ff,
                          material0,
                          material1,
                          folder,
                          force_k=[-1., -1],
                          verbose=False,
                          anisotropy=False):
    """ Recreate  the simplest effective medium theory recreating a composite
    material as the weighted sum of both materials (matrix=material0, and
    inclusions=material1) using ff: [float] the filling factor.

    Arguments:
       ff,
       material0,
       material1,
       eV,
       folder,
       force_k=[-1., -1],
       verbose=False,
       anisotropy=False

     Returns:

    """
    eps0_r, eps0_i = load_material_dat(material0,
                                       folder=None,
                                       is_nm=True,
                                       verbose=False,
                                       has_draw=False)
    eps1_r, eps1_i = load_material_dat(material1,
                                       folder=None,
                                       is_nm=True,
                                       verbose=False,
                                       has_draw=False)

    eps0 = eps0_r + 1j * eps0_i
    eps1 = eps1_r + 1j * eps1_i

    emt_pd = ff * eps1 + (1. - ff) * eps0
    if anisotropy:
        emt_pp = eps0 * eps1 / ((1 - ff) * eps1 + ff * eps0)
        return emt_pd, emt_pp
    else:
        return emt_pd
