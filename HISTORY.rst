=======
History
=======

0.0.1 (2021-11-14)
------------------

* Start new development from scratch
* Generation of 'old' folder with the previous
* Change 'from py_lipss' to 'from py_lipss.old'
* Remove numpyhacks -> shapes
