# Analysis of files for tutorial
<a name="Title"></a>

* Generation of light.
    
    * Angular incidence

* Generation of materials.
    
    * Transparent medium
    
    * Absorbent medium

* Generation of gratings

    * binary

    * Generation of grating with f(x)
    
    * fill factor

* Extraction of information:
    
       * Determination of efficiency for diffraction orders.
       
       * Determination of reflectance and transmitance coefficients (r and t). 

* Drawings

      * Fields
      
      * Talbot effect with metallic grating
      
      * Talbot effect with dielectric grating
      
      * Remove incident light to remove interference


* Extracting Polarization information of light beams.
